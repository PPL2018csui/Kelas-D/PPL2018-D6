require 'mongoid'

# Models
class Feedback
  include Mongoid::Document
  include Mongoid::Timestamps
  field :exhibit_id, type: String, default: ""
  field :description, type: String, default: ""
  field :userid, type: String
  field :timestamp, type: Time, default: 0
  validates :exhibit_id, presence: true
end
