require 'mongoid'

# Vote Databases for Succesed Scanned QR Code
class VoteHistory
  include Mongoid::Document
  include Mongoid::Timestamps

  field :user_id,       type: String
  field :exhibit_id,    type: String
  field :code,          type: String
  field :timestamp,     type: Time

  validates :user_id, presence: true
end
