require 'mongoid'

# Vote Databases for Succesed Scanned QR Code
class User
  include Mongoid::Document
  include Mongoid::Timestamps

  field :user_id,       type: String
  field :user_icon,     type: String,   default: ""
  field :user_name,     type: String,   default: ""
  field :user_email,    type: String,   default: ""
  field :password,      type: String
  field :vote_limit,    type: Integer,  default: 0
  field :logged_in,     type: Integer,  default: 0
  field :last_login,    type: Time,     default: Time.now + (60 * 60 * 7)
  field :administrator, type: Integer,  default: 1
  field :verified,      type: Integer,  default: 0

  def login?
    self.logged_in == 1
  end

  def logout!
    self.logged_in = 0
    self.save
  end

  def login!
    self.logged_in = 1
    self.last_login = Time.now + (60 * 60 * 7)
    self.save
  end

  def vote?
    self.vote_limit > 0
  end

  def vote!
    self.vote_limit = self.vote_limit - 1
    self.save
  end

  def verify!
    self.verified = 1
    self.vote_limit = 5
    self.save
  end

  def verified?
    self.verified == 1
  end

  def addvote!
    self.vote_limit = self.vote_limit + 1
    self.save
  end
end
