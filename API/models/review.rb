require 'mongoid'

# Models
class Review
  include Mongoid::Document
  field :exhibit_id, type: String
  field :user_id, type: String
  field :rating, type: Integer, default: 0
  field :description, type: String, default: ""
  field :timestamp, type: Time, default: 0
  validates :exhibit_id, presence: true
end
