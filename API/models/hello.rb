require 'mongoid'

# Models
class Hello
  include Mongoid::Document

  field :user, type: String

  validates :user, presence: true

  index(user: 'text')
end
