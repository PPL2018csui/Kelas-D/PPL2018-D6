require 'mongoid'

# Media Databases for Succesed Scanned QR Code
class Media
  include Mongoid::Document

  field :exhibit_id,    type: String
  field :type,          type: Integer
  field :link,          type: String
end
