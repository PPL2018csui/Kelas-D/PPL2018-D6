require 'mongoid'

# Vote Databases for validating QR Code
class Vote
  include Mongoid::Document

  field :exhibit_id,    type: String
  field :code,          type: String

  validates :exhibit_id, presence: true
end
