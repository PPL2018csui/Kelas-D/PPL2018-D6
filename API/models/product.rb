require 'mongoid'

# Models
class Product
  include Mongoid::Document
  field :exhibit_id, type: String
  field :exhibit_name, type: String
  field :icon, type: String, default: ""
  field :description, type: String, default: ""
  field :view, type: Integer, default: 0

  def inc!
    self.view = self.view + 1
    self.save
  end
end
