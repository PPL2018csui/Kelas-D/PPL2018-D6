# This class is used for control list of products
class ProductController < ApplicationController
  before do
    content_type :json
  end

  get '/' do
    payload = []
    products = Product.all
    if products.nil?
      return {
        status: 'EMPTY',
        message: "No Products",
        payload: []
      }.to_json
    end

    products.each do |product|
      payload.push({ exhibit_id: product.exhibit_id, exhibit_name: product.exhibit_name, icon: product.icon })
    end

    return {
      status: 'SUCCESS',
      message: 200,
      payload: payload
    }.to_json
  end

  get '/:id' do
    product = Product.find_by(exhibit_id: params[:id])
    if product.nil?
      return {
        status: 'EMPTY',
        message: "There is no product with this id",
        payload: []
      }.to_json
    else
      product.inc!
      payload = {
        exhibit_id: params[:id],
        exhibit_name: product.exhibit_name,
        icon: product.icon,
        description: product.description,
        view: product.view
      }
      return {
        status: 'SUCCESS',
        message: 200,
        payload: payload
      }.to_json
    end
  end

  post '/edit' do
    exhibit_id = params[:exhibit_id]
    unless exhibit_id.nil?
      exhibit_id.gsub!(/[\\"]/, '')
    end
    exhibit_name = params[:exhibit_name]
    unless exhibit_name.nil?
      exhibit_name.gsub!(/[\\"]/, '')
    end
    description = params[:desc]
    unless description.nil?
      description.gsub!(/[\\"]/, '')
    end
    icon = params[:icon]
    unless icon.nil?
      icon.gsub!(/[\\"]/, '')
    end

    puts exhibit_id
    puts exhibit_name
    puts description
    puts icon

    exhibit = Product.find_by(exhibit_id: exhibit_id)
    if exhibit.nil?
      return {
        status: 'FAILED',
        message: "There is no product with this id"
      }.to_json
    end

    exhibit.update(description: description, icon: icon)

    return {
      status: "SUCCESS",
      message: "Update success",
      payload: {
        description: description,
        icon: icon
      }
    }.to_json
  end

  post '/addfeedback' do
    exhibit_id = params[:exhibit_id]
    description = params[:desc]
    if exhibit_id.nil?
      message = "One or more parameters is missing"

      return {
        status: "FAILED",
        message: message
      }.to_json
    else
      unless description.nil?
        description.gsub!(/[\\"]/, '')
      end
      exhibit_id.gsub!(/[\\"]/, '')
      message = "Feedback Success"
      Feedback.create(exhibit_id: exhibit_id, description: description, timestamp: Time.now + (60 * 60 * 7))
      return {
        status: "SUCCESS",
        message: message,
        payload: {
          exhibit_id: exhibit_id,
          timestamp: Time.now + (60 * 60 * 7)
        }
      }.to_json
    end
  end
  get '/getfeedback/:exhibit_id' do
    payload = []
    exhibit_id = params[:exhibit_id]
    unless exhibit_id.nil?
      exhibit_id.gsub!(/[\\"]/, '')
    end

    feedback = Feedback.where(exhibit_id: exhibit_id)
    if feedback.nil?
      return {
        status: 'FAILED',
        message: "No Feedback",
        payload: []
      }.to_json
    end

    feedback.each do |h|
      payload.push({ exhibit_id: h.exhibit_id, description: h.description, timestamp: h.timestamp.strftime("%H:%M %p") })
    end

    return {
      status: 'SUCCESS',
      message: 200,
      payload: payload
    }.to_json
  end

  post '/addreview' do
    exhibit_id = params[:exhibit_id]
    user_id = params[:user_id]
    description = params[:desc]
    if exhibit_id.nil?
      message = "One or more parameters is missing"

      return {
        status: "FAILED",
        message: message
      }.to_json
    else
      exhibit_id.gsub!(/[\\"]/, '')
      unless user_id.nil?
        user_id.gsub!(/[\\"]/, '')
      end
      unless description.nil?
        description.gsub!(/[\\"]/, '')
      end
      message = "Review Success"
      Review.create(exhibit_id: exhibit_id, user_id: user_id, rating: 5, description: description, timestamp: Time.now + (60 * 60 * 7))
      return {
        status: "SUCCESS",
        message: message,
        payload: {
          exhibit_id: exhibit_id,
          timestamp: Time.now + (60 * 60 * 7)
        }
      }.to_json
    end
  end

  get '/getreview/:exhibit_id' do
    payload = []
    exhibit_id = params[:exhibit_id]
    unless exhibit_id.nil?
      exhibit_id.gsub!(/[\\"]/, '')
    end
    review = Review.where(exhibit_id: exhibit_id)
    total_review = 0

    if review.nil?
      return {
        status: 'FAILED',
        message: "No Review",
        payload: []
      }.to_json
    end

    review.each do |h|
      total_review += 1
      user = User.find_by(user_id: h.user_id)
      payload.push({  exhibit_id: h.exhibit_id,
                      user_id: h.user_id,
                      user_name: user.user_name,
                      user_icon: user.user_icon,
                      rating: h.rating,
                      description: h.description,
                      timestamp: h.timestamp.strftime("%H:%M %p") })
    end

    return {
      status: 'SUCCESS',
      message: 200,
      total_review: total_review,
      payload: payload
    }.to_json
  end
end
