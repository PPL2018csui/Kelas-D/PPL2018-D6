require "net/http"
# Media Class
class MediaController < ApplicationController
  before do
    content_type :json
  end

  post '/upload' do
    exhibit_id = params[:exhibit_id]
    type = params[:type]
    link = params[:link]

    if exhibit_id.nil? || type.nil? || link.nil?
      return {
        status: "FAILED",
        message: "One or more parameter is missing"
      }.to_json
    end

    exhibit_id.gsub!(/[\\"]/, '')
    type.gsub!(/[\\"]/, '')
    link.gsub!(/[\\"]/, '')
    puts exhibit_id
    puts type
    puts link

    if type == "1"
      media = Media.find_by(type: type, exhibit_id: exhibit_id)
      puts media
      if media.nil?
        Media.create(exhibit_id: exhibit_id, type: type, link: link)
      else
        media.update(link: link)
      end
    else
      Media.create(exhibit_id: exhibit_id, type: type, link: link)
    end

    message = "Upload Success"
    return {
      status: "SUCCESS",
      message: message,
      payload: {
        exhibit_id: exhibit_id,
        type: type,
        link: link
      }
    }.to_json
  end

  get '/list/:exhibit_id' do
    payload = []
    exhibit_id = params[:exhibit_id]
    exhibit_id.gsub!(/[\\"]/, '')

    media = Media.where(exhibit_id: exhibit_id)
    if media.count.zero?
      return {
        status: 'S',
        message: "No Media",
        payload: []
      }.to_json
    end

    media.each do |m|
      if m.type == 1
        payload.insert(0, { type: m.type, link: m.link })
      else
        payload.push({ type: m.type, link: m.link })
      end
    end

    return {
      status: 'SUCCESS',
      message: "Success return list of media",
      payload: payload
    }.to_json
  end

  post '/delete' do
    exhibit_id = params[:exhibit_id]
    link = params[:link]

    if exhibit_id.nil? || link.nil?
      return {
        status: "FAILED",
        message: "One or more parameter is missing"
      }.to_json
    end

    exhibit_id.gsub!(/[\\"]/, '')
    link.gsub!(/[\\"]/, '')

    media = Media.find_by(exhibit_id: exhibit_id, link: link)
    if media.nil?
      return {
        status: "FAILED",
        message: "There are no media with this id",
        payload: {
          exhibit_id: media.exhibit_id,
          type: media.type,
          link: media.link
        }
      }.to_json
    else
      d_exhibit_id = media.exhibit_id
      d_type = media.type
      d_link = media.link

      media.destroy

      return {
        status: "SUCCESS",
        message: "Success delete media",
        payload: {
          exhibit_id: d_exhibit_id,
          type: d_type,
          link: d_link
        }
      }.to_json
    end
  end
end
