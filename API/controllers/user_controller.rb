# Controlling User Flow and Access
class UserController < ApplicationController
  before do
    content_type :json
    require 'digest'
  end

  # Return description of user
  get '/:user_id' do
    user_id = params[:user_id]

    user = User.find_by(user_id: user_id)

    if user.nil?

      message = "Username not found"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    message = "Success retrieve user detail"

    return {
      status: "SUCCESS",
      message: message,
      payload: {
        user_id: user_id,
        last_login: user.last_login,
        admin: user.administrator,
        vote_limit: user.vote_limit,
        verified: user.verified,
        user_name: user.user_name,
        user_icon: user.user_icon,
        user_email: user.user_email
      }
    }.to_json
  end

  # This are for exhibitor only login service
  post '/login' do
    exhibit_id = params[:user_id]
    unless exhibit_id.nil?
      exhibit_id.gsub!(/[\\"]/, '')
    end
    exhibit_pass = params[:password]
    unless exhibit_pass.nil?
      exhibit_pass.gsub!(/[\\"]/, '')
    end

    # For exhibitior, only 1 phone is allowed to login, you cannot login with more than 1 phone. Limit the phone

    user = User.find_by(user_id: exhibit_id)

    if user.nil?
      message = "Username not found"

      # Return
      return {
        status: "FAILED",
        message: message
      }.to_json

    elsif user.login?
      message = "User already logged in on another phone"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    encrypted_pass = Digest::MD5.hexdigest(exhibit_pass)

    if user.password == encrypted_pass

      user.login!
      message = "Success Login!"

      # Return
      return {
        status: 'SUCCESS',
        message: message,
        payload: {
          user_id: exhibit_id,
          administrator: user.administrator
        }
      }.to_json

    else
      message = "Password invalid"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end
  end

  post '/logout' do
    exhibit_id = params[:user_id]
    exhibit_id.gsub!(/[\\"]/, '')
    user = User.find_by(user_id: exhibit_id)

    if user.nil?
      message = "User not found"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    unless user.login?
      message = "You are not logged in"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    user.logout!
    message = 'Success logged out'
    return {
      status: 'SUCCESS',
      message: message
    }.to_json
  end

  post '/editpassword' do
    exhibit_id = params[:user_id]
    unless exhibit_id.nil?
      exhibit_id.gsub!(/[\\"]/, '')
    end
    current_pass = params[:current_pass]
    unless current_pass.nil?
      current_pass.gsub!(/[\\"]/, '')
    end
    new_pass = params[:new_pass]
    unless new_pass.nil?
      new_pass.gsub!(/[\\"]/, '')
    end

    user = User.find_by(user_id: exhibit_id)

    if user.nil?
      message = "User not found"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    encrypted_pass = Digest::MD5.hexdigest(current_pass)
    encrypted_newpass = Digest::MD5.hexdigest(new_pass)

    if encrypted_pass == user.password
      user.update(password: encrypted_newpass)
      user.save

      message = "Change password success!"

      # Return
      return {
        status: 'SUCCESS',
        message: message
      }.to_json

    else

      message = "Old password invalid"

      # Return
      return {
        status: 'FAILED',
        message: message
      }.to_json

    end
  end

  post '/login_google' do
    firebase_id = params[:user_id]
    user_icon = params[:user_icon]
    user_name = params[:user_name]
    user_email = params[:user_email]

    unless firebase_id.nil?
      firebase_id.gsub!(/[\\"]/, '')
    end
    unless user_icon.nil?
      user_icon.gsub!(/[\\"]/, '')
    end
    unless user_name.nil?
      user_name.gsub!(/[\\"]/, '')
    end
    unless user_email.nil?
      user_email.gsub!(/[\\"]/, '')
    end

    # For exhibitior, only 1 phone is allowed to login, you cannot login with more than 1 phone. Limit the phone
    user = User.find_by(user_id: firebase_id)

    if user.nil?
      user = User.create(user_id: firebase_id, password: '', administrator: 0, user_icon: user_icon, user_name: user_name, user_email: user_email)
    else
      user.update(last_login: Time.now + (60 * 60 * 7))
    end

    return {
      status: "SUCCESS",
      message: "Google account login",
      payload: {
        user_id: firebase_id,
        vote_limit: user.vote_limit,
        verified: user.verified,
        admin: user.administrator
      }
    }.to_json
  end

  post '/verify' do
    user_email = params[:user_email]
    unless user_email.nil?
      user_email.gsub!(/[\\"]/, '')
    end

    user = User.find_by(user_email: user_email)

    if user.nil?
      return {
        status: "FAILED",
        message: "No user with this ID"
      }.to_json
    elsif user.verified?
      return {
        status: "SUCCESS",
        message: "Account already verified"
      }.to_json
    else
      user.verify!
    end

    return {
      status: "SUCCESS",
      message: "Success verify account"
    }.to_json
  end
end
