# API Controller for Hello World
class HelloController < ApplicationController
  get '/' do
    "Hello World #{params[:name]}".strip
  end
end
