# Controller for voting flow
class VoteController < ApplicationController
  before do
    content_type :json
  end

  get '/generate' do
    exhibit_id = params[:exhibit_id]

    if exhibit_id.nil?
      message = "One or more parameters is missing"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    data = Time.now.strftime "%Y%m%d%H%M%S" + Random.new.rand(100..999).to_s
    vote = Vote.find_by(exhibit_id: exhibit_id)

    if vote.nil?
      Vote.create(exhibit_id: exhibit_id, code: data)
    else
      vote.update(code: data)
      vote.save
    end
    code = data + exhibit_id

    url = 'https://api.qrserver.com/v1/create-qr-code/?data=' + code
    return {
      status: 'SUCCESS',
      message: 'Generate QR Code Success',
      payload: {
        code: data + exhibit_id,
        image_file: url
      }
    }.to_json
  end

  post '/verify' do
    data = params[:qr_code]
    user_id = params[:user_id]

    if data.nil? || user_id.nil?
      message = "One or more parameters is missing"

      return {
        status: "FAILED",
        message: message,
        payload: {}
      }.to_json
    end

    puts data
    puts user_id

    user_id.gsub!(/[\\"]/, '')
    data.gsub!(/[^0-9A-Za-z]/, '')

    code = data[0..16]
    exhibit_id = data[17..data.length]
    votes = VoteHistory.find_by(user_id: user_id, exhibit_id: exhibit_id)

    if votes.nil?

    else
      puts "FAILED"
      return {
        status: "FAILED",
        message: "You already vote this exhibit!",
        payload: {}
      }.to_json
    end

    user = User.find_by(user_id: user_id)
    exhibit = User.find_by(user_id: exhibit_id)

    puts user
    unless user.vote?
      return {
        status: "FAILED",
        message: "Your vote exceed your limit",
        payload: {}
      }.to_json
    end

    vote = Vote.find_by(exhibit_id: exhibit_id, code: code)

    if vote.nil?
      message = "Invalid QR Code"
      return {
        status: "FAILED",
        message: message,
        payload: {}
      }.to_json
    else
      message = "Verification Success"
      user.vote!
      exhibit.addvote!
      vote.destroy
      VoteHistory.create(user_id: user_id, exhibit_id: exhibit_id, code: code, timestamp: Time.now + (60 * 60 * 7))
      return {
        status: "SUCCESS",
        message: message,
        payload: {
          user_id: user_id,
          exhibit_id: exhibit_id,
          timestamp: Time.now + (60 * 60 * 7)
        }
      }.to_json
    end
  end

  get '/history/:userid' do
    payload = []
    user_id = params[:userid]
    history = VoteHistory.where(user_id: user_id)
    if history.nil?
      return {
        status: 'FAILED',
        message: "No History",
        payload: []
      }.to_json
    end

    history.each do |h|
      product = Product.find_by(exhibit_id: h.exhibit_id)
      payload.push({ exhibit_id: h.exhibit_id, exhibit_name: product.exhibit_name, time: h.timestamp.strftime("%H:%M %p") })
    end

    return {
      status: 'SUCCESS',
      message: 200,
      payload: payload
    }.to_json
  end

  post '/cancel' do
    user_id = params[:user_id]
    exhibit_id = params[:exhibit_id]

    if user_id.nil? || exhibit_id.nil?
      message = "One or more parameters is missing"

      return {
        status: "FAILED",
        message: message
      }.to_json
    end

    user_id.gsub!(/[\\"]/, '')
    exhibit_id.gsub!(/[\\"]/, '')
    user = User.find_by(user_id: user_id)
    exhibit = User.find_by(user_id: exhibit_id)

    history = VoteHistory.find_by(user_id: user_id, exhibit_id: exhibit_id)

    if history.nil?
      message = "History not found"

      return {
        status: "FAILED",
        message: message
      }.to_json
    else
      payload = {
        canceled_user_id: history.user_id,
        canceled_exhibit_id: history.exhibit_id,
        canceled_timestamp: history.timestamp
      }

      history.destroy
      user.addvote!
      exhibit.vote!
      Feedback.find_by(exhibit_id: exhibit_id, userid: user_id).destroy

      return {
        status: "SUCCESS",
        message: "Success cancel vote",
        payload: payload
      }.to_json
    end
  end

  get '/leaderboard' do
    leaderboard = VoteHistory.collection.aggregate([
        { "$group" => {
            "_id" => { "exhibit_id" => "$exhibit_id" },
            "count" => { "$sum" => 1 }
          }
        },
        { "$sort" => {
          "count" => -1
          }
        }
    ])
    on_leaderboard = []

    leaderboard.each do |h|
      on_leaderboard.push(h["_id"]["exhibit_id"])
    end

    payload = []

    leaderboard.each do |h|
      product = Product.find_by(exhibit_id: h["_id"]["exhibit_id"])
      payload.push(exhibit_id: product.exhibit_id, exhibit_name: product.exhibit_name, icon: product.icon, total_vote: h["count"])
    end

    Product.all.each do |p|
      unless on_leaderboard.include?(p.exhibit_id)
        payload.push(exhibit_id: p.exhibit_id, exhibit_name: p.exhibit_name, icon: p.icon, total_vote: 0)
      end
    end

    return {
      status: "SUCCESS",
      message: "Success get leaderboard",
      payload: payload
    }.to_json
  end
end
