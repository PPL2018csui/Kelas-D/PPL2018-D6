# application_controller.rb
class ApplicationController < Sinatra::Base
  # set folder for templates to ../views, but make the path absolute
  # set :views, File.expand_path('../../views', __FILE__)
  # don't enable logging when running tests
  configure :production, :development, :test do
    enable :logging
  end
end
