require File.expand_path('../spec_helper.rb', __dir__)
require File.expand_path('../../controllers/user_controller.rb', __dir__)
require File.expand_path('../../models/user.rb', __dir__)
require 'digest'

describe UserController do
  context "GET to /:user_id" do
    let(:user) { User.create(user_id: "testfound", password: "12345") }
    before { allow(User).to receive(:find_by).with(user_id: "testfound").and_return(user) }
    before { allow(User).to receive(:find_by).with(user_id: "ok").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_id: "testnotfound").and_return(nil) }
    it "return status 200 OK" do
      get '/ok'

      expect(last_response).to be_ok
    end

    it "handle when user id is invalid" do
      get '/testnotfound'

      expect(last_response.body).to include("Username not found")
    end

    it "return user data from database" do
      get '/testfound'

      expect(last_response.body).to include("Success retrieve user detail")
    end
  end

  context "POST to /login" do
    let(:password) { Digest::MD5.hexdigest("12345") }
    let(:user) { User.create(user_id: "testfound", password: password) }
    let(:userlogged) { User.create(user_id: "testlogged", password: password, logged_in: 1) }
    before { allow(User).to receive(:find_by).with(user_id: "ok").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_id: "testfound").and_return(user) }
    before { allow(User).to receive(:find_by).with(user_id: "testnotfound").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_id: "testlogged").and_return(userlogged) }

    it "return status 200 OK" do
      post '/login', user_id: "ok"

      expect(last_response).to be_ok
    end

    it "handle when user not found" do
      post '/login', user_id: "testnotfound", password: "12345"

      expect(last_response.body).to include("Username not found")
    end

    it "handle when user is already logged in" do
      post '/login', user_id: "testlogged", password: "12345"

      expect(last_response.body).to include("User already logged in on another phone")
    end

    it "handle when password is invalid" do
      post '/login', user_id: "testfound", password: "12344"

      expect(last_response.body).to include("Password invalid")
    end

    it "return success when login success" do
      post '/login', user_id: "testfound", password: "12345"

      expect(last_response.body).to include("Success Login!")
    end
  end

  context "POST to /logout" do
    let(:password) { Digest::MD5.hexdigest("12345") }
    let(:user) { User.create(user_id: "testfound", password: password) }
    let(:userlogged) { User.create(user_id: "testlogged", password: password, logged_in: 1) }
    before { allow(User).to receive(:find_by).with(user_id: "ok").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_id: "testfound").and_return(user) }
    before { allow(User).to receive(:find_by).with(user_id: "testnotfound").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_id: "testlogged").and_return(userlogged) }

    it "return status 200 OK" do
      post '/logout', user_id: "ok"

      expect(last_response).to be_ok
    end

    it "handle when user not found" do
      post '/logout', user_id: "testnotfound"

      expect(last_response.body).to include("User not found")
    end

    it "handle when user is not logged in" do
      post '/logout', user_id: "testfound"

      expect(last_response.body).to include("You are not logged in")
    end

    it "return success when logout success" do
      post '/logout', user_id: "testlogged"

      expect(last_response.body).to include("Success logged out")
    end
  end

  context "POST to /editpassword" do
    let(:password) { Digest::MD5.hexdigest("12345") }
    let(:user) { User.create(user_id: "testfound", password: password) }
    let(:userlogged) { User.create(user_id: "testlogged", password: password, logged_in: 1) }
    before { allow(User).to receive(:find_by).with(user_id: "ok").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_id: "testfound").and_return(user) }
    before { allow(User).to receive(:find_by).with(user_id: "testnotfound").and_return(nil) }

    it "return status 200 OK" do
      post '/editpassword', user_id: "ok"

      expect(last_response).to be_ok
    end

    it "handle when user not found" do
      post '/editpassword', user_id: "testnotfound"

      expect(last_response.body).to include("User not found")
    end

    it "handle when old password is invalid" do
      post '/editpassword', user_id: "testfound", current_pass: "12344", new_pass: "54321"

      expect(last_response.body).to include("Old password invalid")
    end

    it "return success when change password success" do
      post '/editpassword', user_id: "testfound", current_pass: "12345", new_pass: "54321"

      expect(last_response.body).to include("Change password success!")
    end
  end

  context "POST to google login" do
    let(:user) { User.create(user_id: "testfound", password: '', administrator: 0, user_name: "test", user_icon: "test") }
    before { allow(User).to receive(:find_by).with(user_id: "testfound").and_return(user) }
    before { allow(User).to receive(:update).with(last_login: Time.now + (60 * 60 * 7)) }
    before { allow(User).to receive(:find_by).with(user_id: "ok").and_return(nil) }

    it "handle when user is already in database" do
      post '/login_google', user_id: "testfound", user_name: "test", user_icon: "test"

      expect(last_response.body).to include("Google account login")
    end

    it "handle when user is nil" do
      post '/login_google', user_id: "ok", user_name: "test", user_icon: "test"

      expect(last_response.body).to include("Google account login")
    end
  end

  context "POST to verify user" do
    let(:user) { User.create(user_id: "testfound", password: '', administrator: 0, user_email: "testfound") }
    let(:verifieduser) { User.create(user_id: "verified", password: '', administrator: 0, verified: 1, user_email: "verified") }
    before { allow(User).to receive(:find_by).with(user_email: "testfound").and_return(user) }
    before { allow(User).to receive(:find_by).with(user_email: "oka").and_return(nil) }
    before { allow(User).to receive(:find_by).with(user_email: "verified").and_return(verifieduser) }

    it "return status 200 OK" do
      post '/verify', user_email: "oka"

      expect(last_response).to be_ok
    end

    it "handle when user is nil" do
      post '/verify', user_email: "oka"

      expect(last_response.body).to include("No user with this ID")
    end

    it "handle when user is already verified" do
      post '/verify', user_email: "verified"

      expect(last_response.body).to include("Account already verified")
    end

    it "success verified user" do
      post '/verify', user_email: "testfound"

      expect(last_response.body).to include("Success verify account")
    end
  end

  context "cleaning" do
    it "clean db" do
      User.where(user_id: "testfound").destroy
      User.where(user_id: "ok").destroy
      User.where(user_id: "verified").destroy
      User.where(user_id: "testlogged").destroy
    end
  end
end
