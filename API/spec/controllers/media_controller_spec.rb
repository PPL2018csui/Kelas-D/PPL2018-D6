require File.expand_path('../spec_helper.rb', __dir__)
require File.expand_path('../../controllers/media_controller.rb', __dir__)
require File.expand_path('../../models/media.rb', __dir__)

describe MediaController do
  context "post to /media/upload" do
    let(:media) { Media.create(exhibit_id: "testexhibit", type: 0, link: "https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/09/25/goods-img/1510095362978668163.jpg") }
    before { allow(Media).to receive(:create) }
    it "return status 200 OK" do
      post '/upload'
      expect(last_response).to be_ok
    end

    it "should handle when no exhibit_id is given" do
      post '/upload', type: 0, link: "https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/09/25/goods-img/1510095362978668163.jpg"
      expect(last_response.body).to include("One or more parameter is missing")
    end

    it "should handle when no media type is given" do
      post '/upload', exhibit_id: "testexhibit", link: "https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/09/25/goods-img/1510095362978668163.jpg"
      expect(last_response.body).to include("One or more parameter is missing")
    end

    it "should handle when no link is given" do
      post '/upload', exhibit_id: "testexhibit", type: 1
      expect(last_response.body).to include("One or more parameter is missing")
    end

    # it "should handle when link is not valid" do
    #   post '/upload', exhibit_id: "testexhibit", type: 1, link: "wwww.facebook.com"
    #   expect(last_response.body).to include("Link is not valid")
    # end

    it "should successed upload media" do
      post '/upload', exhibit_id: "testexhibit", type: 0, link: "https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/09/25/goods-img/1510095362978668163.jpg"
      expect(last_response.body).to include("Upload Success")
    end
  end

  context "get to /media/list" do
    let(:media) { Media.create(exhibit_id: "testsuccess", type: 0, link: "https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/09/25/goods-img/1510095362978668163.jpg") }
    before { allow(Media).to receive(:where).with(exhibit_id: "testsuccess").and_return([media]) }
    before { allow(Media).to receive(:where).with(exhibit_id: "testfailed").and_return([]) }

    it "return status 200 ok" do
      get '/list/testsuccess'
      expect(last_response).to be_ok
    end

    it "handle when exhibit dont have media" do
      get '/list/testfailed'
      expect(last_response.body).to include("No Media")
    end

    it "successed return list of media" do
      get '/list/testsuccess'
      expect(last_response.body).to include("SUCCESS")
    end
  end
end
