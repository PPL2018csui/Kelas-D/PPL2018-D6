require File.expand_path('../spec_helper.rb', __dir__)
require File.expand_path('../../controllers/hello_controller.rb', __dir__)

describe HelloController do
  it 'should allow accessing the home page' do
    get '/'
    # Rspec 2.x
    expect(last_response).to be_ok
  end

  it 'should show Hello World' do
    get '/'
    expect(last_response.body).to include('Hello World')
  end

  it 'displays welcome page if a user exists' do
    get '/', name: 'Laura'

    expect(last_response.body).to include('Hello World Laura')
  end
end
