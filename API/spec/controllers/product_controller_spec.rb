require File.expand_path('../spec_helper.rb', __dir__)
require File.expand_path('../../controllers/product_controller.rb', __dir__)
require File.expand_path('../../models/product.rb', __dir__)

describe ProductController do
  context "GET to /products with no product" do
    before { allow(Product).to receive(:all).and_return(nil) }

    it "returns status 200 OK" do
      get '/'
      expect(last_response).to be_ok
    end

    it "handle when no product" do
      get '/'
      expect(last_response.body).to include("EMPTY")
    end
  end

  context "GET to /products with products" do
    let(:producted) { Product.create(exhibit_id: "testexhibit", exhibit_name: 'testexhibit') }
    before { allow(Product).to receive(:all).and_return([producted]) }

    it "returns status 200 OK" do
      get '/'
      expect(last_response).to be_ok
    end

    it "show a list of product's name and its icon" do
      get '/'
      expect(last_response.body).to include("SUCCESS")
    end
  end

  context "GET to /:id" do
    let(:producted) { Product.create(exhibit_id: "testexhibit", exhibit_name: 'testexhibit') }
    before { allow(Product).to receive(:find_by).with(exhibit_id: ":id").and_return(nil) }
    before { allow(Product).to receive(:find_by).with(exhibit_id: "testexhibit").and_return(producted) }
    before { allow(Product).to receive(:find_by).with(exhibit_id: "100").and_return(nil) }

    it "returns status 200 OK" do
      get '/:id'
      expect(last_response).to be_ok
    end

    it "displays the product's profil" do
      get '/testexhibit'
      expect(last_response.body).to include("SUCCESS")
    end

    it "handle when no product with that id" do
      get '/100'
      expect(last_response.body).to include("There is no product with this id")
    end
  end

  context "POST to /edit" do
    let(:product) { Product.create(exhibit_id: 'testexhibit', exhibit_name: 'testexhibit') }
    before { allow(Product).to receive(:find_by) }
    before { allow(Product).to receive(:find_by).with(exhibit_id: 'testexhibit').and_return(product) }

    it "returns status 200 OK" do
      get '/edit'
      expect(last_response).to be_ok
    end

    it "handle when no product with that id" do
      post '/edit', exhibit_id: 1
      expect(last_response.body).to include("There is no product with this id")
    end

    it "successed update profil" do
      post '/edit', exhibit_id: 'testexhibit', icon: 'testdesc', description: 'testdesc'

      expect(last_response.body).to include("Update success")
    end
  end
end
