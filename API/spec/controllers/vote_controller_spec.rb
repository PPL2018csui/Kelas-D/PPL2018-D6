require File.expand_path('../spec_helper.rb', __dir__)
require File.expand_path('../../controllers/vote_controller.rb', __dir__)
require File.expand_path('../../models/votehistory.rb', __dir__)
require File.expand_path('../../models/vote.rb', __dir__)

# For unit testing at vote controller
describe VoteController do
  context "get to /vote/generate" do
    let(:vote) { Vote.create(exhibit_id: "testexhibit", code: "12345") }
    before { allow(Vote).to receive(:find_by).with(exhibit_id: "testexhibit").and_return(vote) }
    before { allow(Vote).to receive(:find_by).with(exhibit_id: "testnoexhibit").and_return(nil) }
    before { allow(Vote).to receive(:create) }
    it "return status 200 OK" do
      get '/generate'
      expect(last_response).to be_ok
    end

    it "should generate a new random qr code image with no data" do
      get '/generate', exhibit_id: 'testnoexhibit'
      image = 'https://api.qrserver.com/v1/create-qr-code/'
      expect(last_response.body).to include(image)
    end

    it "should generate a new random qr code image with data" do
      get '/generate', exhibit_id: 'testexhibit'
      image = 'https://api.qrserver.com/v1/create-qr-code/'
      expect(last_response.body).to include(image)
    end
  end

  context "post to /vote/verify" do
    let(:vote) { Vote.create(exhibit_id: "testexhibit", code: "20180329133112524") }
    let(:votehistory) { VoteHistory.create(exhibit_id: "test5exhibit", code: "12345", user_id: "test5user", timestamp: Time.now + (60 * 60 * 7)) }
    let(:votealready) { VoteHistory.create(exhibit_id: "exhibitalready", code: "12345", user_id: "testalready", timestamp: Time.now + (60 * 60 * 7)) }
    before { allow(VoteHistory).to receive(:where).with(user_id: "test5user").and_return([votehistory, votehistory, votehistory, votehistory, votehistory]) }
    before { allow(VoteHistory).to receive(:where).with(user_id: "testalready").and_return([votealready]) }
    before { allow(VoteHistory).to receive(:where).with(user_id: "1").and_return(nil) }
    before { allow(Vote).to receive(:find_by).with(exhibit_id: 'testexhibit', code: "20180329133112524").and_return(vote) }
    before { allow(Vote).to receive(:find_by).with(exhibit_id: 'Test', code: "11111111111111111").and_return(nil) }

    it "return status 200 OK" do
      post '/verify'
      expect(last_response).to be_ok
    end

    it "should allow to handle when no qr code is given" do
      post '/verify', user_id: 1, exhibit_id: 'EXH01'

      expect(last_response.body).to include("One or more parameters is missing")
    end

    it "should allow to handle when no user is given" do
      post '/verify', exhibit_id: 'EXH01', qr_code: '109384'

      expect(last_response.body).to include("One or more parameters is missing")
    end

    it "should successed verify the qr code scanned" do
      post '/verify', user_id: 1, qr_code: '20180329133112524testexhibit'

      expect(last_response.body).to include("Verification Success")
    end

    it "should failed verify the qr code scanned" do
      post '/verify', user_id: 1, qr_code: '11111111111111111Test'

      expect(last_response.body).to include("Invalid QR Code")
    end

    it "should failed verify when user already vote 5 time" do
      post '/verify', user_id: 'test5user', qr_code: '11111111111111111test'

      expect(last_response.body).to include("Failed vote because exceed vote limit")
    end

    it "should failed verify when user already vote the exhibit" do
      post '/verify', user_id: 'testalready', qr_code: '11111111111111111exhibitalready'

      expect(last_response.body).to include("You already vote this exhibit!")
    end
  end

  context "get to /vote/history/:userid" do
    let(:producted) { Product.create(exhibit_id: "testexhibit", exhibit_name: 'testexhibit') }
    let(:votehistory) { VoteHistory.create(user_id: "testuser", exhibit_id: 'testexhibit', code: '12345', timestamp: Time.now + (60 * 60 * 7)) }
    before { allow(VoteHistory).to receive(:where).with(user_id: ':userid').and_return(nil) }
    before { allow(VoteHistory).to receive(:where).with(user_id: 'testuser').and_return([votehistory]) }
    before { allow(VoteHistory).to receive(:where).with(user_id: 'testfailed').and_return(nil) }
    before { allow(Product).to receive(:find_by).with(exhibit_id: 'testexhibit').and_return(producted) }

    it "return status 200 OK" do
      get '/history/:userid'
      expect(last_response).to be_ok
    end

    it "handle when user dont have history" do
      get '/history/testfailed'
      expect(last_response.body).to include("No History")
    end

    it "return user's history vote" do
      get '/history/testuser'
      expect(last_response.body).to include("SUCCESS")
    end
  end

  context "post to /vote/cancel" do
    let(:timestamp) { Time.now + (60 * 60 * 7) }
    let(:votehistory) { VoteHistory.create(user_id: "testuser", exhibit_id: 'testexhibit', code: '6998', timestamp: timestamp) }
    before { allow(VoteHistory).to receive(:find_by).with(user_id: 'testuser', exhibit_id: 'testexhibit').and_return(votehistory) }
    before { allow(VoteHistory).to receive(:find_by).with(user_id: 'testhayo', exhibit_id: 'exhibit').and_return(nil) }
    it "return status 200 OK" do
      post '/cancel'
      expect(last_response).to be_ok
    end

    it "handle when no user is given" do
      post '/cancel', exhibit_id: 'testexhibit'
      expect(last_response.body).to include("One or more parameters is missing")
    end

    it "handle when no exhibit is given" do
      post '/cancel', user_id: 'testuser'
      expect(last_response.body).to include("One or more parameters is missing")
    end

    it "handle when history not found" do
      post '/cancel', user_id: 'testhayo', exhibit_id: 'exhibit'
      expect(last_response.body).to include("History not found")
    end

    it "successed cancel vote" do
      post '/cancel', user_id: 'testuser', exhibit_id: 'testexhibit'
      expect(last_response.body).to include("Success cancel vote")
    end
  end

  context "get to /vote/leaderboard" do
    it "return status 200 OK"

    it "successed show leaderboard"
  end

  context "cleaning" do
    it "clean db" do
      Product.where(exhibit_id: 'testexhibit').destroy
      Vote.where(exhibit_id: 'testexhibit').destroy
      VoteHistory.where(exhibit_id: 'testexhibit').destroy
      Product.where(exhibit_id: 'exhibitalready').destroy
      Vote.where(exhibit_id: 'exhibitalready').destroy
      VoteHistory.where(exhibit_id: 'exhibitalready').destroy
      Product.where(exhibit_id: 'test5exhibit').destroy
      Vote.where(exhibit_id: 'test5exhibit').destroy
      VoteHistory.where(exhibit_id: 'test5exhibit').destroy
      expect(true).to be(true)
    end
  end
end
