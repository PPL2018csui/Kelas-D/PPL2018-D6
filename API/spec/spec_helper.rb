require 'simplecov'
require 'mongoid'
require 'sinatra'
require 'sinatra/base'

SimpleCov.profiles.define 'no_vendor_coverage' do
  add_filter 'vendor' # Don't include vendored stuff
end

SimpleCov.start 'no_vendor_coverage'

require 'rack/test'
require 'rspec'
require 'sinatra/base'
require "rspec/json_expectations"
require 'mongoid'

ENV['RACK_ENV'] = 'test'
Mongoid.load!(File.expand_path('../mongoid.yml', __dir__))

require File.expand_path('../controllers/application_controller.rb', __dir__)
Dir.glob('./{models,controllers}/*.rb').each { |file| require file }

Dir.glob('./{models,controllers}/*.rb').each { |file| require file }

# For Rspec 2.x and 3.x
module RSpecMixin
  include Rack::Test::Methods
  def app
    described_class
  end
end

# For RSpec 2.x and 3.x
RSpec.configure { |c| c.include RSpecMixin }
