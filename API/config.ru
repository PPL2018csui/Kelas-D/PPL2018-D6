# config.ru
require 'sinatra'
require 'sinatra/base'
require 'mongoid'

# DB Setup
Mongoid.load! "mongoid.yml"
require 'rest-client'

# pull in the helpers and controllers
$stdout.sync = true

# DB Setup
Mongoid.load! 'mongoid.yml'

require File.expand_path('controllers/application_controller.rb', __dir__)
Dir.glob('./{models,controllers}/*.rb').each { |file| require file }

# map the controllers to routes

map('/') { run HelloController }
map('/user') { run UserController }
map('/products') { run ProductController }
map('/vote') { run VoteController }
map('/media') { run MediaController }
