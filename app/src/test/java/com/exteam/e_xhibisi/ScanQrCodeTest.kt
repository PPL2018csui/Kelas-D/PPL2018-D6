package com.exteam.e_xhibisi

import com.exteam.e_xhibisi.scan_qrcode.Presenter
import com.exteam.e_xhibisi.scan_qrcode.ScanBarcodeActivity
import org.junit.Assert
import org.junit.Test

/**
 * Created by vela on 19/04/18.
 */

class ScanQrCodeTest {

    val scanBarcode = ScanBarcodeActivity()
    val presenter = Presenter(scanBarcode)

    @Test
    fun setView() {
        Assert.assertEquals(
                presenter.view,
                scanBarcode
        )
    }

    @Test
    fun isReady() {
        Assert.assertEquals(presenter.isReady, true)
    }

}
