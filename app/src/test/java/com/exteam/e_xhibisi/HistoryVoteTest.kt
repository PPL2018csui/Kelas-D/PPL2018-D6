package com.exteam.e_xhibisi

import com.exteam.e_xhibisi.history_vote.Presenter
import com.exteam.e_xhibisi.history_vote.HistoryVoteActivity
import org.junit.Assert
import org.junit.Test

/**
 * Created by vela on 03/04/18.
 */
class HistoryVoteTest {

    val historyVote = HistoryVoteActivity()
    val presenter = Presenter(historyVote);

    @Test
    fun setView() {
        Assert.assertEquals(presenter.view, historyVote)
    }

    @Test
    fun isReady() {
        Assert.assertEquals(presenter.isReady, true)
    }
}