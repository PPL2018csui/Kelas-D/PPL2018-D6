package com.exteam.e_xhibisi

import com.exteam.e_xhibisi.product_detail.Presenter
import com.exteam.e_xhibisi.product_detail.ProductDetailActivity
import org.junit.Assert
import org.junit.Test

/**
 * Created by cymon_evo on 20/03/18.
 */

class ProductDetailTest {

    val productDetail = ProductDetailActivity()
    val presenter = Presenter(productDetail);

    @Test
    fun setView() {
        Assert.assertEquals(presenter.view, productDetail)
    }

    @Test
    fun isReady() {
        Assert.assertEquals(presenter.isReady, true)
    }

}