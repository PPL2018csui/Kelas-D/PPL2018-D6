package com.exteam.e_xhibisi

import android.widget.ImageButton
import android.widget.Toast
import com.exteam.e_xhibisi.login.LoginActivity
import com.firebase.ui.auth.AuthUI
import junit.framework.Assert
import java.util.*

class MainTest {

    private val login = LoginActivity()
    private var main = MainActivity()
    private val providers = Arrays.asList<AuthUI.IdpConfig>(AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build())

    fun forceLogin() {
        login.applicationContext.startActivity(AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(this.providers)
                .build())
    }

    fun isReady() {
        main.setUser()
        Assert.assertEquals(main?.findViewById<ImageButton>(R.id.panel_logout), true)
    }

    fun isImageTrue() {
        Assert.assertEquals(main?.findViewById<ImageButton>(R.id.panel_logout)?.resources,
                R.drawable.ic_logout)
    }

    fun forceLogout() {
        AuthUI.getInstance()
                .signOut(main.applicationContext)
                .addOnCompleteListener { Toast.makeText(main.applicationContext, "Logged out", Toast.LENGTH_SHORT).show() }
    }

}