package com.exteam.e_xhibisi.scan_qrcode;

public interface Contract {

    interface View {
        boolean isReady();
    }

    interface Presenter {
        boolean isReady();
    }
}
