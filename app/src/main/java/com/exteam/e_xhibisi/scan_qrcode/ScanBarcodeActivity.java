package com.exteam.e_xhibisi.scan_qrcode;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import com.exteam.e_xhibisi.MainActivity;
import android.Manifest;
import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.AddFeedbackPayload;
import com.exteam.e_xhibisi.data.BaseResult;
import com.exteam.e_xhibisi.data.VerificationResult;
import com.exteam.e_xhibisi.data.Verify;
import com.exteam.e_xhibisi.product_detail.*;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;
import com.google.zxing.Result;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ScanBarcodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler, Contract.View{

    private ZXingScannerView mScannerView;
    private Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new Presenter(this);

        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ScanBarcodeActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    public void reInitiate() {
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.v("TAG", rawResult.getText()); // Prints scan results
        Log.v("TAG", rawResult.getBarcodeFormat().toString());

        mScannerView.stopCamera();

        String user_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null);

        Call<Verify> call1 = new API().verify(rawResult.getText(), user_id);

        call1.enqueue(new Callback<Verify>() {
            @Override
            public void onResponse(Call<Verify> call, Response<Verify> response) {
//                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                if (response.body().getStatus().equals("SUCCESS")) {
                    final VerificationResult result = response.body().getPayload();
                    Toast.makeText(getApplicationContext(), "Vote Success!", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(ScanBarcodeActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.dialog_scan_barcode, null);
                    final EditText feedback = (EditText) mView.findViewById(R.id.feedback);
                    Button submit = (Button) mView.findViewById(R.id.btn_submit_vote);

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(!feedback.getText().toString().isEmpty()){
                                String exhibit_id = result.getExhibitID();
                                String desc = feedback.getText().toString();

                                Call<BaseResult<AddFeedbackPayload>> call2 = new API().addFeedback(exhibit_id, desc);
                                call2.enqueue(new Callback<BaseResult<AddFeedbackPayload>>() {
                                    @Override
                                    public void onResponse(Call<BaseResult<AddFeedbackPayload>> call, Response<BaseResult<AddFeedbackPayload>> response) {
                                        if (response.body().getStatus().equals("SUCCESS")) {
                                            Toast.makeText(getApplicationContext(), "Thanks for supporting developer!", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            finish();
                                        } else {
                                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<BaseResult<AddFeedbackPayload>> call, Throwable t) {
                                        Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }else{
                                Toast.makeText(ScanBarcodeActivity.this, "Feedback must not blank! You need to support developer!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    reInitiate();
                }
            }

            @Override
            public void onFailure(Call<Verify> call, Throwable t) {
//                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void gotoHome() {
        startActivity(new Intent(this, MainActivity.class));
    }


    @Override
    public boolean isReady() {
        return true;
    }
}
