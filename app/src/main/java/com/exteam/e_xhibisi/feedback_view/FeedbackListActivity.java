package com.exteam.e_xhibisi.feedback_view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.api.FeedbackPayload;
import com.exteam.e_xhibisi.data.BaseArrayResult;
import com.exteam.e_xhibisi.factory.DividerFactory;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackListActivity extends AppCompatActivity {

    private LinearLayout container;
    private SwipeRefreshLayout refresh;
    private boolean isLoaded = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.feedback_list);

        final String product_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null);

        container = findViewById(R.id.feedback_container);

        refresh = findViewById(R.id.swipe_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData(product_id);
            }
        });

        fetchData(product_id);
    }

    private void fetchData(String product_id) {
        if (isLoaded) {
            container.removeAllViews();
        }
        loadFeedback(product_id);
        isLoaded = true;
    }

    private void loadFeedback(String product_id) {
        Call<BaseArrayResult<FeedbackPayload>> call = new API().getFeedback(product_id);
        call.enqueue(new Callback<BaseArrayResult<FeedbackPayload>>() {
            @Override
            public void onResponse(Call<BaseArrayResult<FeedbackPayload>> call, Response<BaseArrayResult<FeedbackPayload>> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    List<FeedbackPayload> payload = response.body().getPayload();

                    if (payload.size() < 1) {
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        inflater.inflate(R.layout.nothing_here, container, true);
                    }

                    for (FeedbackPayload item : payload) {
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.factory_feedback_item, container, false);
//                        TextView title = (TextView) layout.getChildAt(0);
                        TextView description = (TextView) layout.getChildAt(0);
                        TextView timestamp = (TextView) layout.getChildAt(1);

//                        title.setText(item.getExhibitID());
                        description.setText(item.getDescription());
                        timestamp.setText(item.getTimestamp());
                        container.addView(layout);

                        container.addView(new DividerFactory(getApplicationContext()).build());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                refresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<BaseArrayResult<FeedbackPayload>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                refresh.setRefreshing(false);
            }
        });
    }
}
