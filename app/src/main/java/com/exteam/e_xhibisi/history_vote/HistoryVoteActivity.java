package com.exteam.e_xhibisi.history_vote;



import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import android.content.Intent;

import com.exteam.e_xhibisi.data.VoteCancel;
import com.exteam.e_xhibisi.data.VoteHistory;
import com.exteam.e_xhibisi.data.VoteHistoryResult;
import com.exteam.e_xhibisi.factory.DividerFactory;
import com.exteam.e_xhibisi.factory.HistoryVoteItemFactory;
import com.exteam.e_xhibisi.factory.RemainingVoteFactory;
import com.exteam.e_xhibisi.factory.ReviewItemFactory;
import com.exteam.e_xhibisi.product_detail.ProductDetailActivity;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vela on 03/04/18.
 */

public class HistoryVoteActivity extends AppCompatActivity implements Contract.View{

    private Presenter presenter;
    private LinearLayout container;
    private SwipeRefreshLayout refresh;
    private boolean isLoaded;
    private String user_id;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_vote);

        presenter = new Presenter(this);

        container = findViewById(R.id.history_vote_container);

        user_id = getIntent().getExtras().getString("user_id");

        refresh = findViewById(R.id.swipe_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData(user_id);
            }
        });

        fetchData(user_id);
    }

    private void fetchData(String id) {
        if (isLoaded) {
            container.removeAllViews();
        }
        loadHistory(id);
        isLoaded = true;
    }

    private void loadHistory(String user_id) {
        Call<VoteHistoryResult> call = new API().getVoteHistory(user_id);

        call.enqueue(new Callback<VoteHistoryResult>() {
            @Override
            public void onResponse(Call<VoteHistoryResult> call, Response<VoteHistoryResult> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    List<VoteHistory>  result = response.body().getPayload();
                    if (result.size() < 1) {
                        LinearLayout root = findViewById(R.id.history_vote_container);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        inflater.inflate(R.layout.nothing_here, root, true);
                    }

                    String user_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                            .getString(SharedPreferenceUtil.AUTH_ID, null);

                    for (VoteHistory history : result ) {
                        HistoryVoteItemFactory layout = new HistoryVoteItemFactory(HistoryVoteActivity.this);
                        setProductButton(layout.getProductButton(), history.getId());
                        setCancelButton(layout.getCancelButton(), user_id, history.getId());
                        RelativeLayout view = layout
                                .setName(history.getName())
                                .setReview(history.getFeedbackText())
                                .setTime(history.getTime())
                                .build();
                        ((LinearLayout) findViewById(R.id.history_vote_container)).addView(view);

                        ((LinearLayout) findViewById(R.id.history_vote_container)).addView(
                                new DividerFactory(HistoryVoteActivity.this).build());

                    }
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                refresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<VoteHistoryResult> call, Throwable t) {
//                t.printStackTrace();
                refresh.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setProductButton(Button button, final String product_id) {
        final AppCompatActivity temp = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoProductPage(product_id);
            }
        });
    }

    public void gotoProductPage(String product_id) {
        Bundle extras = new Bundle();
        extras.putString("product_id", product_id);
        startActivity(new Intent(this, ProductDetailActivity.class).putExtras(extras));
    }

    public void setCancelButton(Button button, final String user_id, final String exhibit_id) {
        final AppCompatActivity temp = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(temp);
                builder.setMessage("Are you sure?");
                builder.setCancelable(true);

                builder.setPositiveButton(
                        "YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                cancel(user_id, exhibit_id);
                            }
                        });

                builder.setNegativeButton(
                        "NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorOrange));
                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorDarkAccent2));
            }
        });
    }

    public void cancel(final String user_id, final String exhibit_id) {
        Call<VoteCancel> call = new API().cancelVote(user_id, exhibit_id);

        call.enqueue(new Callback<VoteCancel>() {
            @Override
            public void onResponse(Call<VoteCancel> call, Response<VoteCancel> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                    fetchData(user_id);
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VoteCancel> call, Throwable t) {
//                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public boolean isReady() {return true;}
}