package com.exteam.e_xhibisi.history_vote;

public interface Contract {

    interface View {
        boolean isReady();
    }

    interface Presenter {
        boolean isReady();
    }
}
