package com.exteam.e_xhibisi.factory;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.ProductEdit;
import com.exteam.e_xhibisi.data.VoteCancel;
import com.exteam.e_xhibisi.data.VoteHistory;
import com.exteam.e_xhibisi.login.LoginActivity;
import com.exteam.e_xhibisi.product_detail.ProductDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vela on 03/04/18.
 */

public class HistoryVoteItemFactory extends RelativeLayout {
    public static final int PRODUCT_NAME_IDX = 0;
    public static final int PRODUCT_VOTE_TIMESTAMP_IDX = 1;
    public static final int PRODUCT_FEEDBACK_IDX = 2;

    private RelativeLayout layout;
    private TextView product_name;
    private TextView vote_timestamp;
    private TextView product_feedback;

    public HistoryVoteItemFactory(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (RelativeLayout) inflater.inflate(R.layout.factory_history_vote_item, this, true);

        layout = (RelativeLayout) layout.getChildAt(0);
        product_name = (TextView) layout.getChildAt(HistoryVoteItemFactory.PRODUCT_NAME_IDX);
        vote_timestamp = (TextView) layout.getChildAt(HistoryVoteItemFactory.PRODUCT_VOTE_TIMESTAMP_IDX);
        product_feedback = (TextView) layout.getChildAt(HistoryVoteItemFactory.PRODUCT_FEEDBACK_IDX);

    }

    public void setProductButton(final Context context, final String product_id) {
        ((Button) findViewById(R.id.show_product_detail_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoDetailProduct(context, product_id);
            }
        });
    }

    public Button getProductButton() {
        return findViewById(R.id.show_product_detail_button);
    }

    public Button getCancelButton() {
        return findViewById(R.id.cancel_vote_button);
    }

    public void gotoDetailProduct(Context context, String id) {
        Bundle extras = new Bundle();
        extras.putString("product_id", id);
        context.startActivity(new Intent(context, ProductDetailActivity.class).putExtras(extras));
    }

    public HistoryVoteItemFactory setName(String text) {
        this.product_name.setText(text);
        return this;
    }

    private String getName() {
        return this.product_name.getText().toString();
    }

    public HistoryVoteItemFactory setTime(String text) {
        this.vote_timestamp.setText(text);
        return this;
    }

    private String getTime() {
        return this.vote_timestamp.getText().toString();
    }

    public HistoryVoteItemFactory setReview(String text) {
        this.product_feedback.setText(text);
        return this;
    }

    private String getReview() {
        return this.product_feedback.getText().toString();
    }

    public HistoryVoteItemFactory build() {
        return this;
    }

}
