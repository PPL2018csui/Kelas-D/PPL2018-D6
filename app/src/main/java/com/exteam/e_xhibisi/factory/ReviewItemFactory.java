package com.exteam.e_xhibisi.factory;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exteam.e_xhibisi.R;
import com.squareup.picasso.Picasso;

public class ReviewItemFactory extends RelativeLayout {

    private static final int REVIEW_PIC_IDX = 0;
    private static final int REVIEW_TITLE_IDX = 1;
    private static final int REVIEW_TEXT_IDX = 2;

    private RelativeLayout layout;
    private ImageView review_pic;
    private TextView review_title;
    private TextView review_text;

    public ReviewItemFactory(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (RelativeLayout) inflater.inflate(R.layout.factory_review_item, this, true);

        layout = (RelativeLayout) layout.getChildAt(0);
        review_pic = (ImageView) layout.getChildAt(ReviewItemFactory.REVIEW_PIC_IDX);
        review_title = (TextView) layout.getChildAt(ReviewItemFactory.REVIEW_TITLE_IDX);
        review_text = (TextView) layout.getChildAt(ReviewItemFactory.REVIEW_TEXT_IDX);
    }

    public ReviewItemFactory setImage(Uri imageURL) {
        Picasso.get().load(imageURL).into(this.review_pic);
        return this;
    }

    public ReviewItemFactory setImageDrawable() {
        this.review_pic.setImageDrawable(getResources().getDrawable(R.drawable.profile));
        return this;
    }

    private ImageView getImage() {
        return this.review_pic;
    }

    public ReviewItemFactory setTitle(String text) {
        this.review_title.setText(text);
        return this;
    }

    private String getTitle() {
        return this.review_title.getText().toString();
    }

    public ReviewItemFactory setReview(String text) {
        this.review_text.setText(text);
        return this;
    }

    private String getReview() {
        return this.review_text.getText().toString();
    }

    public ReviewItemFactory build() {
        return this;
    }
}
