package com.exteam.e_xhibisi.factory;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exteam.e_xhibisi.R;
import com.squareup.picasso.Picasso;

public class ProductItemNewFactory extends LinearLayout {

    public static final int PRODUCT_PIC_IDX = 0;
    public static final int PRODUCT_TITLE_IDX = 1;

    private LinearLayout layout;
    private ImageView product_icon;
    private TextView product_name;

    public ProductItemNewFactory(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (LinearLayout) inflater.inflate(R.layout.factory_product_item_new, this, true);

        layout = (LinearLayout) layout.getChildAt(0);
        product_icon = (ImageView) layout.getChildAt(ProductItemNewFactory.PRODUCT_PIC_IDX);
        product_name = (TextView) layout.getChildAt(ProductItemNewFactory.PRODUCT_TITLE_IDX);
    }

    public ProductItemNewFactory setImage(Uri imageURL) {
        Picasso.get().load(imageURL).into(this.product_icon);
        return this;
    }

    private ImageView getImage() {
        return this.product_icon;
    }

    public ProductItemNewFactory setTitle(String text) {
        this.product_name.setText(text);
        return this;
    }

    private String getTitle() {
        return this.product_name.getText().toString();
    }

    public ProductItemNewFactory build() {
        return this;
    }
}