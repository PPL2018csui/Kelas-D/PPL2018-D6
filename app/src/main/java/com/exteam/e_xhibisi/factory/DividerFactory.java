package com.exteam.e_xhibisi.factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.exteam.e_xhibisi.R;

public class DividerFactory extends LinearLayout {

    private LinearLayout layout;
    private View divider;

    public DividerFactory(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (LinearLayout) inflater.inflate(R.layout.factory_divider, this, true);

        divider = (View) layout.getChildAt(0);
    }

    public View build() {
        return this;
    }
}
