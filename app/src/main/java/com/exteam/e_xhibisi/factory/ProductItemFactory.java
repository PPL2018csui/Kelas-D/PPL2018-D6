package com.exteam.e_xhibisi.factory;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exteam.e_xhibisi.R;
import com.squareup.picasso.Picasso;

public class ProductItemFactory extends LinearLayout {

    public static final int PRODUCT_PIC_IDX = 0;
    public static final int PRODUCT_TITLE_IDX = 1;

    private LinearLayout layout;
    private ImageView product_pic;
    private TextView product_title;

    public ProductItemFactory(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (LinearLayout) inflater.inflate(R.layout.factory_product_item, this, true);

        layout = (LinearLayout) layout.getChildAt(0);
        product_pic = (ImageView) layout.getChildAt(ProductItemFactory.PRODUCT_PIC_IDX);
        product_title = (TextView) layout.getChildAt(ProductItemFactory.PRODUCT_TITLE_IDX);
    }

    public ProductItemFactory setImage(Uri imageURL) {
        Picasso.get().load(imageURL).into(this.product_pic);
        return this;
    }

    private ImageView getImage() {
        return this.product_pic;
    }

    public ProductItemFactory setTitle(String text) {
        this.product_title.setText(text);
        return this;
    }

    private String getTitle() {
        return this.product_title.getText().toString();
    }

    public ProductItemFactory build() {
        return this;
    }
}
