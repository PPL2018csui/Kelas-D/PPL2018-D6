package com.exteam.e_xhibisi.factory;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exteam.e_xhibisi.R;
import com.squareup.picasso.Picasso;


public class RemainingVoteFactory extends LinearLayout {

    private static final int REMAINING_VOTE_IDX = 0;

    private LinearLayout layout;
    private TextView remaining_vote;

    public RemainingVoteFactory(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (LinearLayout) inflater.inflate(R.layout.factory_remaining_vote, this, true);

        layout = (LinearLayout) layout.getChildAt(0);
        remaining_vote = (TextView) layout.getChildAt(RemainingVoteFactory.REMAINING_VOTE_IDX);

    }

    public RemainingVoteFactory setRemmainingVote(String text) {
        this.remaining_vote.setText(text);
        return this;
    }

    private String getRemainingVote() {
        return this.remaining_vote.getText().toString();
    }

    public RemainingVoteFactory build() {
        return this;
    }
}
