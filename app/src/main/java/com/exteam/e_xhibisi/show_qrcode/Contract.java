package com.exteam.e_xhibisi.show_qrcode;

public class Contract {

    interface View {
        boolean isReady();
    }

    interface Presenter {
        boolean isReady();
    }
}
