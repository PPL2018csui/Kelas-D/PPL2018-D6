package com.exteam.e_xhibisi.show_qrcode;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.Generate;
import com.exteam.e_xhibisi.factory.ReviewItemFactory;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowQRCodeActivity extends AppCompatActivity implements Contract.View {

    private Presenter presenter;
    private ImageView qrcode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_qrcode);

        presenter = new Presenter(this);

        Intent intent = getIntent();
        String product_name = intent.getExtras().getString("product_name");

        ((TextView) findViewById(R.id.header)).setText(product_name);

        qrcode = (ImageView) findViewById(R.id.generated_qrcode);

        Button button = (Button) findViewById(R.id.button_generate_qrcode);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generate();
            }
        });

        generate();

    }

    private void generate() {
        String exhibit_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null);
        Call<Generate> call = new API().getGeneratedQR(exhibit_id);

        call.enqueue(new Callback<Generate>() {
            @Override
            public void onResponse(Call<Generate> call, Response<Generate> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    String uri = response.body().getPayload().getImageFile();
                    Picasso.get().load(uri).into(qrcode);
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Generate> call, Throwable t) {
//                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean isReady() {
        return true;
    }
}
