package com.exteam.e_xhibisi.product_manage;

public interface Contract {

    interface View {
        boolean isReady();
    }

    interface Presenter {
        boolean isReady();
    }
}
