package com.exteam.e_xhibisi.product_manage;

public class Presenter implements Contract.Presenter {

    private Contract.View view;

    public Presenter(Contract.View view) {
        this.view = view;
    }

    @Override
    public boolean isReady() {
        return view.isReady();
    }

    public Contract.View getView() {
        return this.view;
    }
}
