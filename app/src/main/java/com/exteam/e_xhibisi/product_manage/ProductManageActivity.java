package com.exteam.e_xhibisi.product_manage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.DialogInterface;

import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.BaseResult;
import com.exteam.e_xhibisi.data.MediaPayload;
import com.exteam.e_xhibisi.product_detail.ProductDetailActivity;
import com.exteam.e_xhibisi.data.ProductDetail;
import com.exteam.e_xhibisi.data.ProductDetailResult;
import com.exteam.e_xhibisi.data.ProductEdit;
import com.exteam.e_xhibisi.data.ProductEditResult;
import com.exteam.e_xhibisi.factory.DividerFactory;
import com.exteam.e_xhibisi.factory.ReviewItemFactory;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;
import com.squareup.picasso.Picasso;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.exteam.e_xhibisi.data.BaseArrayResult;
import com.exteam.e_xhibisi.data.MediaPayload;

import java.io.FileInputStream;
import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import android.view.LayoutInflater;
import android.view.Menu;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.graphics.Bitmap;
import android.widget.Button;
import android.graphics.BitmapFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductManageActivity extends AppCompatActivity implements Contract.View {

    private Presenter presenter;
    private ProductDetailResult result;
    private ProductEditResult editResult;
    private MediaPayload mediaResult;
    private ImageView profile_pic;
    private ImageView pickPhoto1;
    private LinearLayout slider_container;


    private String currentImageUri;
    private final int SELECT_PHOTO = 1;
    private final int SELECT_MEDIA1 = 2;
    final AppCompatActivity temp = this;
    private Map<String, Uri> edittedMedia;
    private Uri cimageUri;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_manage);
        edittedMedia = new HashMap();

        Map config = new HashMap();
        config.put("cloud_name", "the-e-xteam");
        config.put("api_key","726434842368784");
        config.put("api_secret","AVwj2NGHy3mDxarA-KNl8DgeUv8");
        try {
            MediaManager.init(this, config);
        } catch (Exception e) {

        }

        presenter = new Presenter(this);

        final String product_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null);

        Call<ProductDetail> call = new API().getProduct(product_id);

        Button pickImage = (Button) findViewById(R.id.button_pick);
        Button upload_photo1 = (Button) findViewById(R.id.upload_photo1);
        pickPhoto1 = (ImageView) findViewById(R.id.photo_pick1);
        Button saveButton = (Button) findViewById(R.id.save_button);
        Button uploadVideo = (Button) findViewById(R.id.upload_video_button);

        call.enqueue(new Callback<ProductDetail>() {
            @Override
            public void onResponse(Call<ProductDetail> call, Response<ProductDetail> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    result = response.body().getPayload();
                    profile_pic = (ImageView) findViewById(R.id.product_pic);
                    Picasso.get().load(Uri.parse(result.getIcon())).into(profile_pic);
                    currentImageUri = result.getIcon();
                    ((EditText) findViewById(R.id.product_description)).setText(result.getDescription());
                    ((TextView) findViewById(R.id.product_title)).setText(result.getExhibitName());
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductDetail> call, Throwable t) {
//                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });

        final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        slider_container = findViewById(R.id.slider_container);
        Call<BaseArrayResult<MediaPayload>> call2 = new API().getMedia(product_id);
        call2.enqueue(new Callback<BaseArrayResult<MediaPayload>>() {
            @Override
            public void onResponse(Call<BaseArrayResult<MediaPayload>> call, Response<BaseArrayResult<MediaPayload>> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    List<MediaPayload> payload = response.body().getPayload();

                    for (MediaPayload item : payload) {
                            final String link = item.getLink();

                            if (item.getType() == SharedPreferenceUtil.MEDIA_IMAGE) {
                                final ImageView next = (ImageView) inflater.inflate(R.layout.trailer_next, slider_container, false);
                                Picasso.get().load(Uri.parse(item.getLink())).into(next);
                                slider_container.addView(next);
                                next.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(temp);
                                        builder.setMessage("Do you want to delete this image?");
                                        builder.setCancelable(true);

                                        builder.setPositiveButton(
                                                "YES",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        delete(product_id, link);
                                                        slider_container.removeView(next);
                                                    }
                                                });

                                        builder.setNegativeButton(
                                                "NO",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });

                                        AlertDialog alert = builder.create();
                                        alert.show();
                                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorOrange));
                                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorDarkAccent2));
                                    }
                                });
                            }
                        }
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<BaseArrayResult<MediaPayload>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });

        pickPhoto1 = (ImageView) findViewById(R.id.photo_pick1);

        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });

        upload_photo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_MEDIA1);
            }
        });

        uploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText link_insert = (EditText) findViewById(R.id.video_field);

                String youtubeId = extractYoutubeId(link_insert.getText().toString());

                if (youtubeId == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(temp);
                    builder.setMessage("Oops, seems like the link is broken! Please try again with working Youtube link...");
                    builder.setCancelable(true);

                    builder.setNegativeButton(
                            "GOT IT!",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();
                    alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorDarkAccent2));
                } else {
                    Call<BaseResult<MediaPayload>> call = new API().uploadMedia(product_id, SharedPreferenceUtil.MEDIA_VIDEO, link_insert.getText().toString());

                    call.enqueue(new Callback<BaseResult<MediaPayload>>(){
                        @Override
                        public void onResponse(Call<BaseResult<MediaPayload>> call, Response<BaseResult<MediaPayload>> response) {

                            if(response.body().getStatus().equals("SUCCESS")) {
                                mediaResult = response.body().getPayload();
                                Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResult<MediaPayload>> call, Throwable t) {
//                        t.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String time = getCurrentTimeStamp();
                for (Map.Entry<String, Uri> entry : edittedMedia.entrySet()) {

                    String requestId = MediaManager.get().upload(entry.getValue())
                            .option("resource_type", "auto")
                            .option("overwrite", true)
                            .option("invalidate", true)
                            .option("public_id", entry.getKey() + product_id + time).dispatch();

                    if (entry.getKey() != "profile_icon") {
                        String source =  entry.getKey() + product_id + time + ".jpg";
                        currentImageUri = MediaManager.get().url().generate(source);

                        Call<BaseResult<MediaPayload>> call = new API().uploadMedia(product_id, SharedPreferenceUtil.MEDIA_IMAGE, currentImageUri);
                        call.enqueue(new Callback<BaseResult<MediaPayload>>(){
                            @Override
                            public void onResponse(Call<BaseResult<MediaPayload>> call, Response<BaseResult<MediaPayload>> response) {

                                if(response.body().getStatus().equals("SUCCESS")) {
                                    mediaResult = response.body().getPayload();
                                    Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResult<MediaPayload>> call, Throwable t) {
//                                t.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

                String description = ((EditText) findViewById(R.id.product_description)).getText().toString();
                String icon = MediaManager.get().url().generate("profile_icon" + product_id + time + ".jpg");
                String name = ((EditText) findViewById(R.id.product_title)).getText().toString();
                System.out.println(product_id);

                Call<ProductEdit> call = new API().editProduct(product_id, name, description, icon);

                call.enqueue(new Callback<ProductEdit>() {
                    @Override
                    public void onResponse(Call<ProductEdit> call, Response<ProductEdit> response) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            editResult = response.body().getPayload();
                            Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductEdit> call, Throwable t) {
//                        t.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    public String extractYoutubeId(String videoUrl) {
        final String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
        if (videoUrl == null || videoUrl.trim().length() <= 0)
            return null;

        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return null;
    }


    public void delete(String exhibit_id, String link) {
        Call<BaseResult<MediaPayload>> call = new API().deleteMedia(exhibit_id, SharedPreferenceUtil.MEDIA_IMAGE, link);

            call.enqueue(new Callback<BaseResult<MediaPayload>>(){
                @Override
                public void onResponse(Call<BaseResult<MediaPayload>> call, Response<BaseResult<MediaPayload>> response) {

                    if(response.body().getStatus().equals("SUCCESS")) {
                        mediaResult = response.body().getPayload();
                        Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BaseResult<MediaPayload>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                }
            });
    }

    public String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    final Uri imageUri = imageReturnedIntent.getData();
                    Picasso.get().load(imageUri).into(profile_pic);
                    edittedMedia.put("profile_icon", imageUri);
                    currentImageUri = imageUri.toString();
                }
                break;
            case SELECT_MEDIA1:
                if (resultCode == RESULT_OK) {
                    final Uri imageUri = imageReturnedIntent.getData();
                    Picasso.get().load(imageUri).into(pickPhoto1);
                    cimageUri = imageUri;
                    edittedMedia.put("image_photo_1", imageUri);
                    currentImageUri = imageUri.toString();
                }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean isReady() {
        return true;
    }
}
