package com.exteam.e_xhibisi

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.exteam.e_xhibisi.api.API
import com.exteam.e_xhibisi.data.*
import com.exteam.e_xhibisi.factory.ProductItemNewFactory
import com.exteam.e_xhibisi.feedback_view.FeedbackListActivity
import com.exteam.e_xhibisi.history_vote.HistoryVoteActivity
import com.exteam.e_xhibisi.login.LoginActivity
import com.exteam.e_xhibisi.product_detail.ProductDetailActivity
import com.exteam.e_xhibisi.scan_qrcode.ScanBarcodeActivity
import com.exteam.e_xhibisi.util.SharedPreferenceUtil
import com.firebase.ui.auth.AuthUI
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private var root: GridLayout? = null
    private var horizontal_root: LinearLayout? = null
    private var vertical_root: LinearLayout? = null
    private val TAB_HOME = 0
    private val TAB_LEADERBOARD = 1
    private var currentTab = 0
    private var content: View? = null
    private var contentScroll: Int = 0
    private var isContentLoaded: Boolean = false
    private var leaderboard: View? = null
    private var leaderboardScroll: Int = 0
    private var isLeaderboardLoaded: Boolean = false
    private var refresh: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard_new)

        // Set action bar
        checkRole()
        // Set navigation bar
        loadPanel()

        gotoHome()

        refresh = findViewById<SwipeRefreshLayout>(R.id.swipe_refresh)
        refresh?.setOnRefreshListener {
            fetchData()
            updateState()
        }
    }

    override fun onResume() {
        super.onResume()
//        Toast.makeText(this, "State updated!", Toast.LENGTH_SHORT).show()
        updateState()
    }

    fun updateState() {
        val user_id = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null)

        val call = API().getUserData(user_id)
        call.enqueue(object : Callback<UserData> {
            override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                if (response.body()?.status.equals("SUCCESS")) {
                    val result = response.body()?.payload

                    val editor = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE).edit()
                    editor.putString(SharedPreferenceUtil.AUTH_ID, result?.userID)
                    editor.putString(SharedPreferenceUtil.AUTH_NAME, result?.userName)
                    editor.putString(SharedPreferenceUtil.AUTH_ICON, result?.userIcon)
                    editor.putString(SharedPreferenceUtil.AUTH_EMAIL, result?.userEmail)
                    editor.putInt(SharedPreferenceUtil.AUTH_ROLE, result!!.admin)
                    editor.putInt(SharedPreferenceUtil.AUTH_VERIFIED, result.verified)
                    editor.putString(SharedPreferenceUtil.AUTH_LAST_LOGIN, result.lastLogin)
                    editor.putInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, result.voteLimit)
                    editor.apply()

                    val user_role = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                            .getInt(SharedPreferenceUtil.AUTH_ROLE, -1)

                    val verified = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                            .getInt(SharedPreferenceUtil.AUTH_VERIFIED, -1)

                    if (user_role == SharedPreferenceUtil.AUTH_ROLE_ADMIN) {
                        setExhibitor()
                    } else {
                        if (verified == SharedPreferenceUtil.AUTH_VERIFIED_TRUE) {
                            setUser()
                        }
                    }
                }
                else {
                    Toast.makeText(applicationContext, response.body()?.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<UserData>, t: Throwable) {
//                t.printStackTrace()
                Toast.makeText(applicationContext, "Network error! Check your connection!", Toast.LENGTH_SHORT).show()
            }
        })
    }

    @SuppressLint("WrongViewCast")
    fun setUser() {
        val vote_left = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                .getInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, -1)
        val verified = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                .getInt(SharedPreferenceUtil.AUTH_VERIFIED, SharedPreferenceUtil.AUTH_VERIFIED_FALSE)
        if (verified == SharedPreferenceUtil.AUTH_VERIFIED_FALSE) {
            findViewById<TextView>(R.id.footer_text).text = resources.getString(R.string.footer_voter_unverified)
        } else {
            findViewById<TextView>(R.id.footer_text).text = resources.getString(R.string.footer_voter_text, vote_left)
        }
        findViewById<TextView>(R.id.footer_text).setOnClickListener {
            // Goto My Vote History
            gotoVoteHistory()
        }
        findViewById<TextView>(R.id.footer_button).text = resources.getString(R.string.footer_voter_button)
        findViewById<Button>(R.id.footer_button).setOnClickListener {
            // Goto Vote
            if (verified == SharedPreferenceUtil.AUTH_VERIFIED_FALSE) {
                verifyPrompt()
            } else {
                gotoScanQRCode()
            }
        }
    }

    @SuppressLint("WrongViewCast")
    fun setExhibitor() {
        val vote_left = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                .getInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, -1)
        findViewById<TextView>(R.id.footer_text).text = resources.getString(R.string.footer_exhibitor_text, vote_left)
        findViewById<TextView>(R.id.footer_text).setOnClickListener {
            // Goto Feedback
            gotoFeedback()
        }
        findViewById<TextView>(R.id.footer_button).text = resources.getString(R.string.footer_exhibitor_button)
        findViewById<Button>(R.id.footer_button).setOnClickListener {
            // Goto Exhibitor's App
//            gotoExhibitorPage()
            val product_id = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                    .getString(SharedPreferenceUtil.AUTH_ID, null)
            gotoDetailProduct(product_id)
        }
    }

    fun verifyPrompt() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("You need to activate this account before you can participate in this exhibition!")
        builder.setCancelable(true)

        builder.setPositiveButton("GOT IT!") {
            dialog, id -> dialog.cancel()
        }

        val alert = builder.create()
        alert.show()
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.colorDarkAccent2))
    }

    fun loadPanel() {
        findViewById<TextView>(R.id.home_button).setOnClickListener {
            gotoHome()
        }
        findViewById<TextView>(R.id.leaderboard_button).setOnClickListener {
            gotoLeaderboard()
        }
        findViewById<TextView>(R.id.logout_button).setOnClickListener {
            logout()
        }
    }

    fun fetchData() {
        if (this.currentTab == TAB_HOME) {
            root?.removeAllViews()
            loadProductList()
        } else {
            horizontal_root?.removeAllViews()
            vertical_root?.removeAllViews()
            loadLeaderboard()
        }
    }

    fun loadProductList() {
        root = findViewById(R.id.grid_container_new)
        var call = API().getProductList()

        call.enqueue(object : Callback<ProductList> {
            override fun onResponse(call: Call<ProductList>, response: Response<ProductList>) {
                if (response.body()?.status.equals("SUCCESS")) {
                    var result = response.body()?.payload
                    result?.forEach {
                        var item = ProductItemNewFactory(applicationContext)
                                .setImage(Uri.parse(it.icon))
                                .setTitle(it.name)
                                .build()
                        item.setOnClickListener(object : View.OnClickListener {
                            override fun onClick(v: View) {
                                gotoDetailProduct(it.id)
                            }
                        })
                        root?.addView(item)
                    }
                    isContentLoaded = true
                }
                else {
                    Toast.makeText(applicationContext, response.body()!!.message, Toast.LENGTH_SHORT).show()
                }
                refresh?.isRefreshing = false
            }

            override fun onFailure(call: Call<ProductList>, t: Throwable) {
//                t.printStackTrace()
                Toast.makeText(applicationContext, "Network error! Check your connection!", Toast.LENGTH_SHORT).show()
                refresh?.isRefreshing = false
            }
        })
    }

    fun loadLeaderboard() {
        horizontal_root = findViewById(R.id.horizontal_container_new)
        vertical_root = findViewById(R.id.vertical_container_new)
        var call = API().getLeaderboard()

        call.enqueue(object : Callback<BaseArrayResult<LeaderboardPayload>> {
            override fun onResponse(call: Call<BaseArrayResult<LeaderboardPayload>>, response: Response<BaseArrayResult<LeaderboardPayload>>) {
                if (response.body()?.status.equals("SUCCESS")) {
                    val result = response.body()?.payload
                    var increment: Int = 1
                    result?.forEach {
                        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val layout: RelativeLayout
                        val image: ImageView
                        val badge: ImageView
                        val title: TextView
                        val developer: TextView
                        val rank: TextView
                        val total: TextView

                        if (increment < 4) {
//                            System.out.println("Top: " + increment);
                            layout = (inflater.inflate(R.layout.factory_leaderboard_top, horizontal_root, false) as RelativeLayout)
                            image = layout.getChildAt(0) as ImageView
                            badge = layout.getChildAt(1) as ImageView
                            if (increment == 1) {
                                badge.setImageResource(R.drawable.one)
                            } else if (increment == 2) {
                                badge.setImageResource(R.drawable.two)
                            } else {
                                badge.setImageResource(R.drawable.three)
                            }
                            title = layout.getChildAt(2) as TextView
                            developer = layout.getChildAt(3) as TextView
                            total = layout.getChildAt(4) as TextView
                        } else {
//                            System.out.println("Down: " + increment);
                            layout = (inflater.inflate(R.layout.factory_leaderboard_other, vertical_root, false) as RelativeLayout)
                            rank = layout.getChildAt(0) as TextView
                            rank.text = increment.toString()
                            image = layout.getChildAt(1) as ImageView
                            title = (layout.getChildAt(2) as RelativeLayout).getChildAt(0) as TextView
                            developer = (layout.getChildAt(2) as RelativeLayout).getChildAt(1) as TextView
                            total = layout.getChildAt(3) as TextView
                        }
                        Picasso.get().load(it.icon).into(image)
                        title.text = it.exhibitName
                        developer.text = it.exhibitID
                        total.text = resources.getString(R.string.counter_votes, it.vote)

                        if (increment < 4) {
                            horizontal_root?.addView(layout)
                        } else {
                            vertical_root?.addView(layout)
                        }

                        increment += 1
                    }
                    isLeaderboardLoaded = true
                }
                else {
//                    Toast.makeText(applicationContext, "Error: " + response.toString(), Toast.LENGTH_SHORT).show()
                }
                findViewById<TextView>(R.id.leaderboard_top).text = resources.getString(R.string.leaderboard_top_app)
                refresh?.isRefreshing = false
            }

            override fun onFailure(call: Call<BaseArrayResult<LeaderboardPayload>>, t: Throwable) {
//                t.printStackTrace()
                Toast.makeText(applicationContext, "Network error! Check your connection!", Toast.LENGTH_SHORT).show()
                refresh?.isRefreshing = false
            }
        })
    }

    fun isContainerLoaded() : Boolean {
        var isReady = false
        var container = findViewById<ScrollView>(R.id.container)
        if (container.childCount > 0) {
            isReady = true
        }
        return isReady
    }

    @SuppressLint("WrongViewCast")
    fun setTab(tab: Int) {
        val root = findViewById<ScrollView>(R.id.container)
        if (tab == TAB_LEADERBOARD) {
            this.currentTab = TAB_LEADERBOARD
            this.contentScroll = root.scrollY
            findViewById<TextView>(R.id.header).text = resources.getString(R.string.tab_leaderboard)
            findViewById<TextView>(R.id.leaderboard_button).setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkAccent2))
            findViewById<TextView>(R.id.home_button).setBackgroundColor(ContextCompat.getColor(this, R.color.colorHeader))

            if (leaderboard == null) {
                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                this.leaderboard = inflater.inflate(R.layout.leaderboard_new, root, false)
            }
            root.removeAllViews()
            root.addView(this.leaderboard)
            root.scrollY = this.leaderboardScroll
        } else {
            this.currentTab = TAB_HOME
            this.leaderboardScroll = root.scrollY
            findViewById<TextView>(R.id.header).text = resources.getString(R.string.tab_home)
            findViewById<TextView>(R.id.home_button).setBackgroundColor(ContextCompat.getColor(this, R.color.colorDarkAccent2))
            findViewById<TextView>(R.id.leaderboard_button).setBackgroundColor(ContextCompat.getColor(this, R.color.colorHeader))

            if (content == null) {
                val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                this.content = inflater.inflate(R.layout.dashboard_home, root, false)
            }
            root.removeAllViews()
            root.addView(this.content)
            root.scrollY = this.contentScroll
        }
    }

    fun gotoHome() {
        setTab(TAB_HOME)
        if (isContainerLoaded()) {
            if (!isContentLoaded) {
                loadProductList()
            }
        } else {
            Toast.makeText(this, "Failed to load data", Toast.LENGTH_SHORT).show()
        }
    }

    fun gotoLeaderboard() {
        setTab(TAB_LEADERBOARD)
        if (isContainerLoaded()) {
            if (!isLeaderboardLoaded) {
                loadLeaderboard()
            }
        } else {
            Toast.makeText(this, "Failed to load data", Toast.LENGTH_SHORT).show()
        }
    }

    fun gotoFeedback() {
        startActivity(Intent(this, FeedbackListActivity::class.java))
    }

    fun gotoDetailProduct(id: String) {
        val extras = Bundle()
        extras.putString("product_id", id)
        startActivity(Intent(this, ProductDetailActivity::class.java).putExtras(extras))
    }

    fun gotoVoteHistory() {
        val user_id = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null)
        val extras = Bundle()
        extras.putString("user_id", user_id)
        startActivity(Intent(this, HistoryVoteActivity::class.java).putExtras(extras))
    }

    fun gotoScanQRCode() {
        startActivity(Intent(this, ScanBarcodeActivity::class.java))
    }

    fun gotoLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    fun checkRole() {
        val role = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE).getInt(SharedPreferenceUtil.AUTH_ROLE, 0)
        if (role == SharedPreferenceUtil.AUTH_ROLE_USER) {
            setUser()
        } else if (role == SharedPreferenceUtil.AUTH_ROLE_ADMIN) {
            setExhibitor()
        } else {
            clear()
            finish()
        }
    }

    fun logout() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure?")
        builder.setCancelable(true)

        builder.setPositiveButton("LOGOUT") {
            dialog, id ->
            val role = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE)
                    .getInt(SharedPreferenceUtil.AUTH_ROLE, 0)
            if (role == SharedPreferenceUtil.AUTH_ROLE_USER) {
                clear()
                logoutFirebase()
            } else if (role == SharedPreferenceUtil.AUTH_ROLE_ADMIN) {
                logoutExhibitor()
            } else {
                clear()
                finish()
            }
        }

        builder.setNegativeButton("CANCEL") {
            dialog, id -> dialog.cancel()
        }

        val alert = builder.create()
        alert.show()
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.colorOrange))
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.colorDarkAccent2))
    }

    fun logoutExhibitor() {
        val userID = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE).getString(SharedPreferenceUtil.AUTH_ID, null)
        val call = API().logout(userID)

        call.enqueue(object : Callback<LogoutExhibitor> {
            override fun onResponse(call: Call<LogoutExhibitor>, response: Response<LogoutExhibitor>) {
                if (response.body()!!.status == "SUCCESS") {
                    clear()
//                    Toast.makeText(applicationContext, "Logged out!", Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    Toast.makeText(applicationContext, response.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<LogoutExhibitor>, t: Throwable) {
//                t.printStackTrace()
                Toast.makeText(applicationContext, "Network error! Check your connection!", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun logoutFirebase() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
//                    Toast.makeText(applicationContext, "Logged out!", Toast.LENGTH_SHORT).show()
                }
//        (applicationContext as Activity).finish()
        finish()
    }

    fun clear() {
        val editor = getSharedPreferences(SharedPreferenceUtil.AUTH, Context.MODE_PRIVATE).edit()
        editor.remove(SharedPreferenceUtil.AUTH_ID)
        editor.remove(SharedPreferenceUtil.AUTH_NAME)
        editor.remove(SharedPreferenceUtil.AUTH_ICON)
        editor.remove(SharedPreferenceUtil.AUTH_EMAIL)
        editor.putInt(SharedPreferenceUtil.AUTH_ROLE, -1)
        editor.putInt(SharedPreferenceUtil.AUTH_VERIFIED, -1)
        editor.putInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, -1)
        editor.remove(SharedPreferenceUtil.AUTH_LAST_LOGIN)
        editor.apply()
    }
}
