package com.exteam.e_xhibisi.api;

public class FeedbackPayload {

    private String exhibit_id;
    private String description;
    private String timestamp;

    public void setExhibitID(String exhibit_id) {
        this.exhibit_id = exhibit_id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
