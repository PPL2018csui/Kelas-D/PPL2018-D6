package com.exteam.e_xhibisi.api;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;

public class VideoRequestHandler extends RequestHandler {
    public String SCHEME_VIDEO = "video";

    @Override
    public boolean canHandleRequest(Request data) {
        String scheme = data.uri.getScheme();
        return SCHEME_VIDEO.equals(scheme);
    }

    @Nullable
    @Override
    public Result load(Request request, int networkPolicy) {
        Bitmap bm = ThumbnailUtils.createVideoThumbnail(request.uri.getPath(), MediaStore.Video.Thumbnails.MINI_KIND);
        return new Result(bm, Picasso.LoadedFrom.NETWORK);
    }

}
