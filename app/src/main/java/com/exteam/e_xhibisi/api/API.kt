package com.exteam.e_xhibisi.api

import com.exteam.e_xhibisi.data.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class API {

    private val EXHIBISI_API_BASE_URL = "https://exhibisi-prod.herokuapp.com/"
    private val EXHIBISI_API_PRODUCTION_URL = "https://exhibisi-prod.herokuapp.com/"
    private val EXHIBISI_API_STAGING_URL = "https://exhibisi-staging.herokuapp.com/"

    private val httpClient = OkHttpClient.Builder()

    private val exhibisiBuilder = Retrofit.Builder()
            .baseUrl(EXHIBISI_API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()

    fun getUserData(userID: String?): Call<UserData> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getUserData(userID)
    }

    fun getProductList(): Call<ProductList> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .productList
    }

    fun getProduct(id: String): Call<ProductDetail> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getProduct(id)
    }

    fun addFeedback(exhibitorID: String?, desc: String?): Call<BaseResult<AddFeedbackPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .addFeedback(exhibitorID, desc)
    }

    fun getFeedback(exhibitorID: String?): Call<BaseArrayResult<FeedbackPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getFeedback(exhibitorID)
    }

    fun getMedia(exhibitorID: String?) : Call<BaseArrayResult<MediaPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getMedia(exhibitorID)
    }

    fun uploadMedia(exhibitorID: String?, type: Int, imageUri: String?) : Call<BaseResult<MediaPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .uploadMedia(exhibitorID, type, imageUri)
    }

    fun deleteMedia(exhibitorID: String?, type: Int, imageUri: String?) : Call<BaseResult<MediaPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .deleteMedia(exhibitorID, type, imageUri)
    }

    fun getGeneratedQR(exhibitorID: String?): Call<Generate> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getGeneratedQR(exhibitorID)
    }

    fun verify(codeQR: String?, userID: String?): Call<Verify> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .verify(codeQR, userID)
    }

    fun cancelVote(userID: String?, exhibitorID: String?): Call<VoteCancel> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .cancelVote(userID, exhibitorID)
    }

    fun addReview(exhibitorID: String?, userID: String?, desc: String?): Call<BaseResult<AddReviewPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .addReview(exhibitorID, userID, desc);
    }

    fun getReview(exhibitorID: String?): Call<BaseArrayResult<ReviewPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getReview(exhibitorID);
    }

    fun editProduct(exhibitID: String?, exhibitName: String?, description: String?, icon: String?): Call<ProductEdit> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .editProduct(exhibitID, exhibitName, description, icon)
    }

    fun getVoteHistory(userID: String?): Call<VoteHistoryResult>? {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .getVoteHistory(userID)
    }

    fun login(userID: String?, password: String?): Call<LoginExhibitor> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .login(userID, password)
    }

    fun loginGoogle(userID: String?, userName: String?, userIcon: String?, userEmail: String?): Call<BaseResult<GooglePayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .loginGoogle(userID, userName, userIcon, userEmail)
    }

    fun logout(userID: String?): Call<LogoutExhibitor> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .logout(userID)
    }

    fun getLeaderboard(): Call<BaseArrayResult<LeaderboardPayload>> {
        return exhibisiBuilder.create(ExhibisiAPIClient::class.java)
                .leaderboard
    }
}