package com.exteam.e_xhibisi.api;

import com.exteam.e_xhibisi.data.AddReviewPayload;
import com.exteam.e_xhibisi.data.BaseArrayResult;
import com.exteam.e_xhibisi.data.BaseResult;
import com.exteam.e_xhibisi.data.AddFeedbackPayload;
import com.exteam.e_xhibisi.data.Generate;
import com.exteam.e_xhibisi.data.GooglePayload;
import com.exteam.e_xhibisi.data.LeaderboardPayload;
import com.exteam.e_xhibisi.data.LoginExhibitor;
import com.exteam.e_xhibisi.data.LogoutExhibitor;
import com.exteam.e_xhibisi.data.MediaPayload;
import com.exteam.e_xhibisi.data.ProductDetail;
import com.exteam.e_xhibisi.data.ProductEdit;
import com.exteam.e_xhibisi.data.ProductList;
import com.exteam.e_xhibisi.data.ReviewPayload;
import com.exteam.e_xhibisi.data.UserData;
import com.exteam.e_xhibisi.data.Verify;
import com.exteam.e_xhibisi.data.VoteCancel;
import com.exteam.e_xhibisi.data.VoteHistoryResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ExhibisiAPIClient {

    @GET("/user/{user_id}")
    Call<UserData> getUserData(@Path("user_id") String userID);

    @GET("/products")
    Call<ProductList> getProductList();

    @GET("/products/{id}")
    Call<ProductDetail> getProduct(@Path("id") String id);

    @Multipart
    @POST("/products/addfeedback")
    Call<BaseResult<AddFeedbackPayload>> addFeedback(@Part("exhibit_id") String exhibitID,
                                                     @Part("desc") String desc);

    @GET("/products/getfeedback/{exhibit_id}")
    Call<BaseArrayResult<FeedbackPayload>> getFeedback(@Path("exhibit_id") String exhibitID);

    @Multipart
    @POST("/media/upload")
    Call<BaseResult<MediaPayload>> uploadMedia(@Part("exhibit_id") String exhibitID,
                                  @Part("type") int type,
                                  @Part("link") String imageUri);

    @Multipart
    @POST("/media/delete")
    Call<BaseResult<MediaPayload>> deleteMedia(@Part("exhibit_id") String exhibitID,
                                  @Part("type") int type,
                                  @Part("link") String imageUri);

    @GET("/media/list/{exhibit_id}")
    Call<BaseArrayResult<MediaPayload>> getMedia(@Path("exhibit_id") String exhibitID);

    @Multipart
    @POST("/products/edit")
    Call<ProductEdit> editProduct(@Part("exhibit_id") String exhibitID,
                                  @Part("exhibit_name") String exhibitName,
                                  @Part("desc") String description,
                                  @Part("icon") String icon);

    @Multipart
    @POST("/products/addreview")
    Call<BaseResult<AddReviewPayload>> addReview(@Part("exhibit_id") String exhibitID,
                                                 @Part("user_id") String userID,
                                                 @Part("desc") String desc);

    @GET("/products/getreview/{exhibit_id}")
    Call<BaseArrayResult<ReviewPayload>> getReview(@Path("exhibit_id") String exhibitID);

    @GET("/vote/generate?")
    Call<Generate> getGeneratedQR(@Query("exhibit_id") String exhibitID);

    @Multipart
    @POST("/vote/verify")
    Call<Verify> verify(@Part("qr_code") String codeQR,
                        @Part("user_id") String userID);

    @Multipart
    @POST("/user/logout")
    Call<LogoutExhibitor> logout(@Part("user_id") String userID);

    @Multipart
    @POST("/user/login")
    Call<LoginExhibitor> login(@Part("user_id") String userID,
                               @Part("password") String password);

    @Multipart
    @POST("/user/login_google")
    Call<BaseResult<GooglePayload>> loginGoogle(@Part("user_id") String userID,
                                                @Part("user_name") String userName,
                                                @Part("user_icon") String userIcon,
                                                @Part("user_email") String userEmail);

    @GET("/vote/history/{user_id}")
    Call<VoteHistoryResult> getVoteHistory(@Path("user_id") String userID);

    @Multipart
    @POST("/vote/cancel")
    Call<VoteCancel> cancelVote(@Part("user_id") String userID,
                                @Part("exhibit_id") String exhibit_id);

    @GET("/vote/leaderboard")
    Call<BaseArrayResult<LeaderboardPayload>> getLeaderboard();
}