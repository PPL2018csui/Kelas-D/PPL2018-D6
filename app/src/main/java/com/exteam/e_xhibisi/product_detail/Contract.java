package com.exteam.e_xhibisi.product_detail;

/**
 * Created by cymon_evo on 20/03/18.
 */

public interface Contract {

    interface View {
        boolean isReady();
    }

    interface Presenter {
        boolean isReady();
    }
}
