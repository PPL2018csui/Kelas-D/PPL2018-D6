package com.exteam.e_xhibisi.product_detail;

/**
 * Created by cymon_evo on 20/03/18.
 */

public class Presenter implements Contract.Presenter {

    private Contract.View view;

    public Presenter(Contract.View view) {
        this.view = view;
    }

    @Override
    public boolean isReady() {
        return view.isReady();
    }

    public Contract.View getView() {
        return this.view;
    }
}
