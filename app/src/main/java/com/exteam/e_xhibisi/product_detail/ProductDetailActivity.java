package com.exteam.e_xhibisi.product_detail;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.AddReviewPayload;
import com.exteam.e_xhibisi.data.BaseArrayResult;
import com.exteam.e_xhibisi.data.BaseResult;
import com.exteam.e_xhibisi.data.MediaPayload;
import com.exteam.e_xhibisi.data.ProductDetail;
import com.exteam.e_xhibisi.data.ProductDetailResult;
import com.exteam.e_xhibisi.data.ReviewPayload;
import com.exteam.e_xhibisi.factory.DividerFactory;
import com.exteam.e_xhibisi.factory.ReviewItemFactory;
import com.exteam.e_xhibisi.product_manage.ProductManageActivity;
import com.exteam.e_xhibisi.show_qrcode.ShowQRCodeActivity;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cymon_evo on 15/03/18.
 */

public class ProductDetailActivity extends AppCompatActivity implements Contract.View {

    private Presenter presenter;
    private SwipeRefreshLayout refresh;
    private boolean isLoaded, isMediaLoaded, isContentLoaded, isReviewLoaded = false;
    private LinearLayout container, slider_container;
    private String product_id, product_name;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);

        presenter = new Presenter(this);

        findViewById(R.id.trailer_image).setVisibility(View.GONE);
        container = findViewById(R.id.review_container);
        slider_container = findViewById(R.id.slider_container);

        final AppCompatEditText review_field = findViewById(R.id.review_field);
        final TextView watcher_field = findViewById(R.id.text_watcher);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                watcher_field.setText(String.valueOf(charSequence.length() + "/" + getResources().getInteger(R.integer.max_review)));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        review_field.addTextChangedListener(watcher);

        Intent intent = getIntent();
        product_id = intent.getExtras().getString("product_id");
        final int vote_left = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, -1);

        findViewById(R.id.review_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vote_left == -1) {
                    verifyPrompt();
                } else {
                    submitReview(product_id);
                }
            }
        });

        refresh = findViewById(R.id.swipe_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData(product_id);
            }
        });

        checkOwner(product_id);
        checkAdmin();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData(product_id);
    }

    @Override
    public boolean isReady() {
        return true;
    }

    private void checkOwner(String id) {
        String user_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null);
        if (user_id.equals(id)) {
            LinearLayout root = findViewById(R.id.footer_container);
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.dashboard_footer, root, true);

            findViewById(R.id.footer_left).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoGenerateQRCode();
                }
            });

            findViewById(R.id.footer_right).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoEditPage();
                }
            });
        }
    }

    private void checkAdmin() {
        int user_role = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getInt(SharedPreferenceUtil.AUTH_ROLE, -1);
        if (user_role == SharedPreferenceUtil.AUTH_ROLE_ADMIN) {
            findViewById(R.id.review_form).setVisibility(View.GONE);
//            findViewById(R.id.review_form_br).setVisibility(View.GONE);
        }
    }

    private void fetchData(String product_id) {
        if (isLoaded) {
            container.removeAllViews();
            slider_container.removeAllViews();
        }
        this.isContentLoaded = false;
        this.isMediaLoaded = false;
        this.isReviewLoaded = false;
        loadMedia(product_id);
        loadData(product_id);
        loadReview(product_id);
        isLoaded = true;
    }

    private void loadData(String product_id) {
        Call<ProductDetail> call = new API().getProduct(product_id);
        call.enqueue(new Callback<ProductDetail>() {
            @Override
            public void onResponse(Call<ProductDetail> call, Response<ProductDetail> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    ProductDetailResult result = response.body().getPayload();
                    ((TextView) findViewById(R.id.header)).setText(result.getExhibitName());
                    product_name = result.getExhibitName();
                    ((TextView) findViewById(R.id.detail_description)).setText(result.getDescription());
                    ((TextView) findViewById(R.id.detail_view_count)).setText(result.getView() + " views");
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                isContentLoaded = true;
                updateLoaderState();
            }

            @Override
            public void onFailure(Call<ProductDetail> call, Throwable t) {
//                t.printStackTrace();
                refresh.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMedia(String product_id) {
        final ImageView trailerImage = findViewById(R.id.trailer_image);
        final WebView displayYoutubeVideo = findViewById(R.id.mWebView);

        final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        Call<BaseArrayResult<MediaPayload>> call = new API().getMedia(product_id);
        call.enqueue(new Callback<BaseArrayResult<MediaPayload>>() {
            @Override
            public void onResponse(Call<BaseArrayResult<MediaPayload>> call, Response<BaseArrayResult<MediaPayload>> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    List<MediaPayload> payload = response.body().getPayload();
                    boolean isFirst = true;

                    for (MediaPayload item : payload) {
                        ImageView first = (ImageView) inflater.inflate(R.layout.trailer_first, slider_container, false);
                        ImageView next = (ImageView) inflater.inflate(R.layout.trailer_next, slider_container, false);

                        if (item.getType() == 1) {

                            String youtubeId = extractYoutubeId(item.getLink());

                            if (youtubeId == null) {
                                ImageView temp;
                                if (isFirst) {
                                    temp = first;
                                    isFirst = false;
                                    displayYoutubeVideo.setVisibility(View.GONE);
                                    trailerImage.setVisibility(View.VISIBLE);
                                    trailerImage.setImageDrawable(getResources().getDrawable(R.drawable.broken));
                                } else {
                                    temp = next;
                                }

                                temp.setImageDrawable(getResources().getDrawable(R.drawable.broken));
                                slider_container.addView(temp);
                                temp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        displayYoutubeVideo.setVisibility(View.GONE);
                                        displayYoutubeVideo.loadUrl("file:///android_asset/nonexistent.html");
                                        trailerImage.setVisibility(View.VISIBLE);
                                        trailerImage.setImageDrawable(getResources().getDrawable(R.drawable.broken));
                                    }
                                });

                                Toast.makeText(getApplicationContext(), "Sorry, video link is broken", Toast.LENGTH_SHORT).show();
                            } else {
                                String videoUrl = "https://www.youtube.com/embed/" + youtubeId;

                                final String frameVideo = "<html><body><iframe width=\""+ "100%" + "\" height=\"" + "285" + "\" src=\"" + videoUrl + "\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe></body></html>";
                                displayYoutubeVideo.setWebViewClient(new WebViewClient() {
                                    @Override
                                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                        return false;
                                    }
                                });
                                WebSettings webSettings = displayYoutubeVideo.getSettings();
                                webSettings.setJavaScriptEnabled(true);
                                webSettings.setMediaPlaybackRequiresUserGesture(false);
                                String thumbnail_url = "http://img.youtube.com/vi/" + youtubeId + "/0.jpg";

                                ImageView temp;
                                if (isFirst) {
                                    temp = first;
                                    isFirst = false;
                                    trailerImage.setVisibility(View.GONE);
                                    displayYoutubeVideo.setVisibility(View.VISIBLE);
                                    displayYoutubeVideo.loadData(frameVideo, "text/html", "utf-8");
                                } else {
                                    temp = next;
                                }

                                Picasso.get().load(Uri.parse(thumbnail_url)).into(temp);
                                slider_container.addView(temp);
                                temp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        trailerImage.setVisibility(View.GONE);
                                        displayYoutubeVideo.setVisibility(View.VISIBLE);
                                        displayYoutubeVideo.loadData(frameVideo, "text/html", "utf-8");
                                    }
                                });
                            }

                        } else {
                            final String link = item.getLink();
                            ImageView temp;

                            if (isFirst) {
                                temp = first;
                                isFirst = false;
                                displayYoutubeVideo.setVisibility(View.GONE);
                                trailerImage.setVisibility(View.VISIBLE);
                                Picasso.get().load(Uri.parse(link)).into(trailerImage);
                            } else {
                                temp = next;
                            }

                            Picasso.get().load(Uri.parse(item.getLink())).into(temp);
                            slider_container.addView(temp);
                            temp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    displayYoutubeVideo.setVisibility(View.GONE);
                                    displayYoutubeVideo.loadUrl("file:///android_asset/nonexistent.html");
                                    trailerImage.setVisibility(View.VISIBLE);
                                    Picasso.get().load(Uri.parse(link)).into(trailerImage);
                                }
                            });
                        }
                    }
                } else {
                    displayYoutubeVideo.setVisibility(View.GONE);
                    trailerImage.setVisibility(View.VISIBLE);
                    trailerImage.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
                    ImageView first = (ImageView) inflater.inflate(R.layout.trailer_first, slider_container, false);
                    first.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
                    slider_container.addView(first);

                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                isMediaLoaded = true;
                updateLoaderState();
            }

            @Override
            public void onFailure(Call<BaseArrayResult<MediaPayload>> call, Throwable t) {
                refresh.setRefreshing(false);
//                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private String extractYoutubeId(String url) throws MalformedURLException {
//        String query = new URL(url).getQuery();
//        String[] param = query.split("&");
//        String id = null;
//        for (String row : param) {
//            String[] param1 = row.split("=");
//            if (param1[0].equals("v")) {
//                id = param1[1];
//            }
//        }
//        return id;
//    }


    public String extractYoutubeId(String videoUrl) {
        final String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
        if (videoUrl == null || videoUrl.trim().length() <= 0)
            return null;

        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return null;
    }


    private void loadReview(String product_id) {
        final TextView total_review = findViewById(R.id.total_review);

        Call<BaseArrayResult<ReviewPayload>> call = new API().getReview(product_id);
        call.enqueue(new Callback<BaseArrayResult<ReviewPayload>>() {
            @Override
            public void onResponse(Call<BaseArrayResult<ReviewPayload>> call, Response<BaseArrayResult<ReviewPayload>> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    List<ReviewPayload> payload = response.body().getPayload();
                    total_review.setText(getResources().getString(R.string.review_count, payload.size()));

                    if (payload.size() < 1) {
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        inflater.inflate(R.layout.nothing_here, container, true);
                    }

                    for (ReviewPayload item : payload) {
                        ReviewItemFactory view = new ReviewItemFactory(getApplicationContext())
                                .setTitle(item.getUserName());
                        if (item.getUserIcon() == null || item.getUserIcon().equals("")) {
                            view.setImageDrawable();
                        } else {
                            view.setImage(Uri.parse(item.getUserIcon()));
                        }
                        view.setReview(item.getDescription()).build();
                        container.addView(view);

                        container.addView(new DividerFactory(getApplicationContext()).build());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                isReviewLoaded = true;
                updateLoaderState();
            }

            @Override
            public void onFailure(Call<BaseArrayResult<ReviewPayload>> call, Throwable t) {
                refresh.setRefreshing(false);
//                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLoaderState() {
        if (isContentLoaded && isMediaLoaded && isReviewLoaded) {
            this.refresh.setRefreshing(false);
        }
    }

    private void submitReview(final String product_id) {
        String user_id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE)
                .getString(SharedPreferenceUtil.AUTH_ID, null);
        String review = ((TextView) findViewById(R.id.review_field)).getText().toString();

        Call<BaseResult<AddReviewPayload>> call = new API().addReview(product_id, user_id, review);
        call.enqueue(new Callback<BaseResult<AddReviewPayload>>() {
            @Override
            public void onResponse(Call<BaseResult<AddReviewPayload>> call, Response<BaseResult<AddReviewPayload>> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
//                    AddReviewPayload payload = (AddReviewPayload) response.body().getPayload();
                    Toast.makeText(getApplicationContext(), "Your review has published!", Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(), response.body().getPayload().toString(), Toast.LENGTH_LONG).show();
                    ((TextView) findViewById(R.id.review_field)).setText("");
                    fetchData(product_id);
                } else {
                    Toast.makeText(getApplicationContext(), "Something error... Please try again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResult<AddReviewPayload>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void verifyPrompt() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You need to activate this account before you can participate in this exhibition!");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "GOT IT!",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorDarkAccent2));
    }

    private void gotoGenerateQRCode() {
        Bundle extras = new Bundle();
        extras.putString("product_id", product_id);
        extras.putString("product_name", product_name);
        startActivity(new Intent(this, ShowQRCodeActivity.class).putExtras(extras));
    }

    private void gotoEditPage() {
        startActivity(new Intent(this, ProductManageActivity.class));
    }
}
