package com.exteam.e_xhibisi.util;

public class SharedPreferenceUtil {

    public static final String AUTH = "auth";
    public static final String AUTH_ID = "user_id";
    public static final String AUTH_NAME = "user_name";
    public static final String AUTH_EMAIL = "user_email";
    public static final String AUTH_ICON = "user_icon";
    public static final String AUTH_ROLE = "user_role";
    public static final String AUTH_VERIFIED = "user_verified";
    public static final String AUTH_LAST_LOGIN = "user_last_login";
    public static final String AUTH_VOTE_LIMIT = "user_vote_limit";

    public static final int AUTH_VERIFIED_TRUE = 1;
    public static final int AUTH_VERIFIED_FALSE = 0;
    public static final int AUTH_ROLE_USER = 0;
    public static final int AUTH_ROLE_ADMIN = 1;
    public static final int AUTH_ROLE_DEVELOPER = 2;
    public static final int MEDIA_IMAGE = 0;
    public static final int MEDIA_VIDEO = 1;
}
