package com.exteam.e_xhibisi.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.MainActivity;
import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.LoginExhibitorResult;
import com.exteam.e_xhibisi.data.UserData;
import com.exteam.e_xhibisi.data.UserDataResult;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;

import org.jetbrains.annotations.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginExhibitor extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_exhibitor);

        redirectIfAuthenticated();

        findViewById(R.id.btn_login_exhibitor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = ((EditText) findViewById(R.id.login_username)).getText().toString();
                String password = ((EditText) findViewById(R.id.login_password)).getText().toString();
                if (verify(username, password)) {
                    loginExhibitor(username, password);
                }
            }
        });

        findViewById(R.id.btn_login_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLoginUser();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        redirectIfAuthenticated();
//        finishAffinity();
    }

    private void loginExhibitor(final String username, String password) {
        Call<com.exteam.e_xhibisi.data.LoginExhibitor> call = new API().login(username, password);

        call.enqueue(new Callback<com.exteam.e_xhibisi.data.LoginExhibitor>() {
            @Override
            public void onResponse(Call<com.exteam.e_xhibisi.data.LoginExhibitor> call, Response<com.exteam.e_xhibisi.data.LoginExhibitor> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    LoginExhibitorResult result = response.body().getPayload();

                    String id = result.getUserID();

                    Call<UserData> callUserData = new API().getUserData(id);

                    callUserData.enqueue(new Callback<UserData>() {
                        @Override
                        public void onResponse(Call<UserData> call, Response<UserData> response) {
                            if (response.body().getStatus().equals("SUCCESS")) {
                                UserDataResult result = response.body().getPayload();

                                SharedPreferences.Editor editor = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE).edit();
                                editor.putString(SharedPreferenceUtil.AUTH_ID, result.getUserID());
                                editor.putString(SharedPreferenceUtil.AUTH_NAME, result.getUserName());
                                editor.putString(SharedPreferenceUtil.AUTH_ICON, result.getUserIcon());
                                editor.putString(SharedPreferenceUtil.AUTH_EMAIL, result.getUserEmail());
                                editor.putInt(SharedPreferenceUtil.AUTH_ROLE, result.getAdmin());
                                editor.putInt(SharedPreferenceUtil.AUTH_VERIFIED, result.getVerified());
                                editor.putString(SharedPreferenceUtil.AUTH_LAST_LOGIN, result.getLastLogin());
                                editor.putInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, result.getVoteLimit());
                                editor.apply();

                                Toast.makeText(getApplicationContext(), "Welcome! " + result.getUserID(), Toast.LENGTH_SHORT).show();

                                gotoHomepage();
                            } else {
                                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserData> call, Throwable t) {
//                            t.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<com.exteam.e_xhibisi.data.LoginExhibitor> call, Throwable t) {
//                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void gotoHomepage() {
        startActivity(new Intent(this, MainActivity.class));
    }

    private void gotoLoginUser() {
        finish();
    }

    private boolean verify(String username, String password) {
        boolean isValidated = false;
        if (username == null || username.equals("")) {
            Toast.makeText(getApplicationContext(), "Please input your username!", Toast.LENGTH_SHORT).show();
        } else if (password == null || password.equals("")) {
            Toast.makeText(getApplicationContext(), "Please input your password!", Toast.LENGTH_SHORT).show();
        } else {
            isValidated = true;
        }
        return isValidated;
    }

    private void redirectIfAuthenticated() {
        String id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE).getString(SharedPreferenceUtil.AUTH_ID, null);
        if (id != null) {
            gotoHomepage();
        }
    }
}
