package com.exteam.e_xhibisi.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.exteam.e_xhibisi.MainActivity;
import com.exteam.e_xhibisi.R;
import com.exteam.e_xhibisi.api.API;
import com.exteam.e_xhibisi.data.BaseResult;
import com.exteam.e_xhibisi.data.GooglePayload;
import com.exteam.e_xhibisi.data.ProductDetail;
import com.exteam.e_xhibisi.data.ProductDetailResult;
import com.exteam.e_xhibisi.data.UserData;
import com.exteam.e_xhibisi.data.UserDataResult;
import com.exteam.e_xhibisi.util.SharedPreferenceUtil;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cymon_evo on 13/03/18.
 */

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 123;
    private List<AuthUI.IdpConfig> providers;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);

        redirectIfAuthenticated();

        this.providers = Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build());

        findViewById(R.id.btn_login_google).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginGoogle();
            }
        });

        findViewById(R.id.btn_login_exhibitor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLoginExhibitor();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        redirectIfAuthenticated();
//        finishAffinity();
    }

    private void loginGoogle() {
        startActivityForResult(
            AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(this.providers)
                    .build(),
            RC_SIGN_IN);
    }

    private void logout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        Toast.makeText(getApplicationContext(), "Logged out", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void delete() {
        AuthUI.getInstance()
                .delete(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Login success
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                final String id = user.getUid();
                final String name = user.getDisplayName();
                final String icon = user.getPhotoUrl().toString();
                final String email = user.getEmail();

                Call<UserData> call1 = new API().getUserData(id);

                call1.enqueue(new Callback<UserData>() {
                    @Override
                    public void onResponse(Call<UserData> call, Response<UserData> response) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            UserDataResult result = response.body().getPayload();

                            SharedPreferences.Editor editor = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE).edit();
                            editor.putString(SharedPreferenceUtil.AUTH_ID, result.getUserID());
                            editor.putString(SharedPreferenceUtil.AUTH_NAME, result.getUserName());
                            editor.putString(SharedPreferenceUtil.AUTH_ICON, result.getUserIcon());
                            editor.putString(SharedPreferenceUtil.AUTH_EMAIL, result.getUserEmail());
                            editor.putInt(SharedPreferenceUtil.AUTH_ROLE, result.getAdmin());
                            editor.putInt(SharedPreferenceUtil.AUTH_VERIFIED, result.getVerified());
                            editor.putString(SharedPreferenceUtil.AUTH_LAST_LOGIN, result.getLastLogin());
                            editor.putInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, result.getVoteLimit());
                            editor.apply();

                            if (result.getVerified() == SharedPreferenceUtil.AUTH_VERIFIED_FALSE) {
                                Toast.makeText(getApplicationContext(), "You're not verified!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Welcome! " + result.getUserName(), Toast.LENGTH_SHORT).show();
                            }

                            gotoHomepage();
                        } else {
                            SharedPreferences.Editor editor = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE).edit();
                            editor.putString(SharedPreferenceUtil.AUTH_ID, id);
                            editor.putString(SharedPreferenceUtil.AUTH_NAME, name);
                            editor.putString(SharedPreferenceUtil.AUTH_ICON, icon);
                            editor.putString(SharedPreferenceUtil.AUTH_EMAIL, email);
                            editor.putInt(SharedPreferenceUtil.AUTH_ROLE, SharedPreferenceUtil.AUTH_ROLE_USER);
                            editor.putInt(SharedPreferenceUtil.AUTH_VERIFIED, SharedPreferenceUtil.AUTH_VERIFIED_FALSE);
                            editor.putString(SharedPreferenceUtil.AUTH_LAST_LOGIN, "");
                            editor.putInt(SharedPreferenceUtil.AUTH_VOTE_LIMIT, -1);
                            editor.apply();

                            Toast.makeText(getApplicationContext(), "You're not verified!", Toast.LENGTH_SHORT).show();

                            Call<BaseResult<GooglePayload>> call2 = new API().loginGoogle(id, name, icon, email);
                            call2.enqueue(new Callback<BaseResult<GooglePayload>>() {
                                @Override
                                public void onResponse(Call<BaseResult<GooglePayload>> call, Response<BaseResult<GooglePayload>> response) {
//                                    Toast.makeText(getApplicationContext(), "Success adding data", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(Call<BaseResult<GooglePayload>> call, Throwable t) {

                                }
                            });
                            gotoHomepage();

                        }
                    }

                    @Override
                    public void onFailure(Call<UserData> call, Throwable t) {
//                        t.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Network error! Check your connection!", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                // Login failed
                Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void gotoHomepage() {
        startActivity(new Intent(this, MainActivity.class));
    }

    private void gotoLoginExhibitor
            () {
        startActivity(new Intent(this, com.exteam.e_xhibisi.login.LoginExhibitor.class));
    }

    private void redirectIfAuthenticated() {
        String id = getSharedPreferences(SharedPreferenceUtil.AUTH, MODE_PRIVATE).getString(SharedPreferenceUtil.AUTH_ID, null);
        if (id != null) {
            gotoHomepage();
        }
    }
}
