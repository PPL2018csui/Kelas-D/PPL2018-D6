package com.exteam.e_xhibisi.data;

public class Generate {

    private String status;
    private String message;
    private GeneratedToken payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(GeneratedToken token) {
        this.payload = token;
    }

    public GeneratedToken getPayload() {
        return this.payload;
    }
}

