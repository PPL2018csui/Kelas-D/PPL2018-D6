package com.exteam.e_xhibisi.data;

public class BaseResult<T> {

    private String status;
    private String message;
    private T payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public T getPayload() {
        return this.payload;
    }
}
