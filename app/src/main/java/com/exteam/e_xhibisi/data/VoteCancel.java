package com.exteam.e_xhibisi.data;

public class VoteCancel {
    private String status;
    private String message;
    private VoteCancelResult payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }



}
