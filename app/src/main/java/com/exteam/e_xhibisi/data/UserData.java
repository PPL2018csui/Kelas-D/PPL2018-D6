package com.exteam.e_xhibisi.data;

public class UserData {

    private String status;
    private String message;
    private UserDataResult payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(UserDataResult result) {
        this.payload = result;
    }

    public UserDataResult getPayload() {
        return this.payload;
    }
}
