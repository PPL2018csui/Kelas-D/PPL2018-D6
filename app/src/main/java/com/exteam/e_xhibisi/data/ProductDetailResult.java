package com.exteam.e_xhibisi.data;

public class ProductDetailResult {

    private String exhibit_id;
    private String exhibit_name;
    private String icon;
    private String description;
    private int view;

    public void setExhibitID(String id) {
        this.exhibit_id = id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setExhibitName(String name) {
        this.exhibit_name = name;
    }

    public String getExhibitName() {
        return this.exhibit_name;
    }

    public void setIcon(String url) {
        this.icon = url;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setView(int value) {
        this.view = value;
    }

    public int getView() {
        return this.view;
    }
}
