package com.exteam.e_xhibisi.data;

public class GeneratedToken {

    private String code;
    private String image_file;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setImageFile(String url) {
        this.image_file = url;
    }

    public String getImageFile() {
        return this.image_file;
    }
}
