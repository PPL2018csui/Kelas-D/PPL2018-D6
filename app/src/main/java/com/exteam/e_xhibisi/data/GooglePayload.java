package com.exteam.e_xhibisi.data;

public class GooglePayload {

    private String user_id;
    private int vote_limit;
    private boolean verified;
    private int administrator;

    public void setUserID(String user_id) {
        this.user_id = user_id;
    }

    public String getString() {
        return this.user_id;
    }

    public void setVoteLimit(int vote_limit) {
        this.vote_limit = vote_limit;
    }

    public int getVoteLimit() {
        return this.vote_limit;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean getVerified() {
        return this.verified;
    }

    public void setAdministrator(int role) {
        this.administrator = role;
    }

    public int getAdministrator() {
        return this.administrator;
    }

}
