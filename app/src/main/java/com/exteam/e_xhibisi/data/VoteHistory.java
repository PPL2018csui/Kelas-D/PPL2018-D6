package com.exteam.e_xhibisi.data;

public class VoteHistory {

    private String exhibit_id;
    private String exhibit_name;
    private String time;
    private String feedback_text;
    private int remaining_vote;

    public void setId(String id) {
        this.exhibit_id = id;
    }

    public String getId() {
        return this.exhibit_id;
    }

    public void setName(String name) {
        this.exhibit_name = name;
    }

    public String getName() {
        return this.exhibit_name;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return this.time;
    }

    public void setFeedbackText(String text) {
        this.feedback_text = text;
    }

    public String getFeedbackText() {
        return this.feedback_text;
    }

    public void setRemainingVote(int value) {
        this.remaining_vote = value;
    }

    public int getRemainingVote() {
        return this.remaining_vote;
    }
}
