package com.exteam.e_xhibisi.data;

public class VoteCancelResult {

    private String user_id;
    private String exhibit_id;
    private String timestamp;

    public void setUserID(String user_id) {
        this.user_id = user_id;
    }

    public String getUserID() {
        return this.user_id;
    }

    public void setExhibitID(String exhibit_id) {
        this.exhibit_id = exhibit_id;
    }

    public String getExhibitID(String exhibit_id) {
        return this.exhibit_id;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
