package com.exteam.e_xhibisi.data;

public class LoginExhibitor {

    private String status;
    private String message;
    private LoginExhibitorResult payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(LoginExhibitorResult result) {
        this.payload = result;
    }

    public LoginExhibitorResult getPayload() {
        return this.payload;
    }
}
