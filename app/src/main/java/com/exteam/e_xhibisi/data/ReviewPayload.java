package com.exteam.e_xhibisi.data;

public class ReviewPayload {

    private String exhibit_id;
    private String user_id;
    private String user_name;
    private String user_icon;
    private int rating;
    private String description;
    private String timestamp;

    public void setExhibitID(String exhibit_id) {
        this.exhibit_id = exhibit_id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setUserID(String user_id) {
        this.user_id = user_id;
    }

    public String getUserID() {
        return this.user_id;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getUserName() {
        return this.user_name;
    }

    public void setUserIcon(String user_icon) {
        this.user_icon = user_icon;
    }

    public String getUserIcon() {
        return this.user_icon;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return this.rating;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
