package com.exteam.e_xhibisi.data;

public class AddReviewPayload {

    private String exhibit_id;
    private String timestamp;

    public void setExhibitID(String exhibit_id) {
        this.exhibit_id = exhibit_id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

}
