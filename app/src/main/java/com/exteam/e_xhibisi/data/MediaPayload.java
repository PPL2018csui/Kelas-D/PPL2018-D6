package com.exteam.e_xhibisi.data;

public class MediaPayload {

    private String exhibit_id;
    private int type;
    private String link;

    public void setExhibitID(String exhibit_id) {
        this.exhibit_id = exhibit_id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return this.link;
    }
}
