package com.exteam.e_xhibisi.data;

import java.util.List;

public class ProductList {

    private String status;
    private int message;
    private List<Product> payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getMessage() {
        return this.message;
    }

    public void setPayload(List<Product> list) {
        this.payload = list;
    }

    public List<Product> getPayload() {
        return this.payload;
    }
}
