package com.exteam.e_xhibisi.data;

public class ProductDetail {

    private String status;
    private String message;
    private ProductDetailResult payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(ProductDetailResult result) {
        this.payload = result;
    }

    public ProductDetailResult getPayload() {
        return this.payload;
    }
}
