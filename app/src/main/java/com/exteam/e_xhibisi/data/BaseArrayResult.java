package com.exteam.e_xhibisi.data;

import java.util.List;

public class BaseArrayResult<T> {

    private String status;
    private String message;
    private List<T> payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(List<T> payload) {
        this.payload = payload;
    }

    public List<T> getPayload() {
        return this.payload;
    }

}
