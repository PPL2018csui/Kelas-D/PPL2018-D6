package com.exteam.e_xhibisi.data;

public class LeaderboardPayload {

    private String exhibit_id;
    private String exhibit_name;
    private String icon;
    private int total_vote;

    public void setExhibitID(String exhibit_id) {
        this.exhibit_id = exhibit_id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setExhibitName(String exhibit_name) {
        this.exhibit_name = exhibit_name;
    }

    public String getExhibitName() {
        return this.exhibit_name;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setVote(int total_vote) {
        this.total_vote = total_vote;
    }

    public int getVote() {
        return this.total_vote;
    }


}
