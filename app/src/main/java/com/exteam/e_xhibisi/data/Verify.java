package com.exteam.e_xhibisi.data;

public class Verify {

    private String status;
    private String message;
    private VerificationResult payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(VerificationResult result) {
        this.payload = result;
    }

    public VerificationResult getPayload() {
        return this.payload;
    }
}
