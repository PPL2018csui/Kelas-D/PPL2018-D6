package com.exteam.e_xhibisi.data;

public class ProductEdit {

    private String status;
    private String message;
    private ProductEditResult payload;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setPayload(ProductEditResult result) {
        this.payload = result;
    }

    public ProductEditResult getPayload() {
        return this.payload;
    }
}
