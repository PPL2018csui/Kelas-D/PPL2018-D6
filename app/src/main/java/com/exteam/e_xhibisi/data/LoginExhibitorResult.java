package com.exteam.e_xhibisi.data;

public class LoginExhibitorResult {

    private String user_id;

    public void setUserID(String user_id) {
        this.user_id = user_id;
    }

    public String getUserID() {
        return this.user_id;
    }
}
