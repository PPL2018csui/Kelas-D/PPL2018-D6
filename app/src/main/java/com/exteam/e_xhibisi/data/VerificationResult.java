package com.exteam.e_xhibisi.data;

public class VerificationResult {

    private String user_id;
    private String exhibit_id;
    private String timestamp;

    public void setUserID(String id) {
        this.user_id = id;
    }

    public String getUserID() {
        return this.user_id;
    }

    public void setExhibitID(String id) {
        this.exhibit_id = id;
    }

    public String getExhibitID() {
        return this.exhibit_id;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
