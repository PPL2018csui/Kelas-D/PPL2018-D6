package com.exteam.e_xhibisi.data;

public class UserDataResult {
    private String user_id;
    private String last_login;
    private int admin;
    private int vote_limit;
    private String user_name;
    private String user_icon;
    private String user_email;
    private int verified;

    public void setUserID(String id) {
        this.user_id = id;
    }

    public String getUserID() {
        return this.user_id;
    }

    public void setLastLogin(String timestamp) {
        this.last_login = timestamp;
    }

    public String getLastLogin() {
        return this.last_login;
    }

    public void setAdmin(int role) {
        this.admin = role;
    }

    public int getAdmin() {
        return this.admin;
    }

    public void setVoteLimit(int vote_limit) {
        this.vote_limit = vote_limit;
    }

    public int getVoteLimit() {
        return this.vote_limit;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getUserName() {
        return this.user_name;
    }

    public void setUserIcon(String user_icon) {
        this.user_icon = user_icon;
    }

    public String getUserIcon() {
        return this.user_icon;
    }

    public void setUserEmail(String user_email) {
        this.user_email = user_email;
    }

    public String getUserEmail() {
        return this.user_email;
    }

    public void setVerified(int verified) {
        this.verified = verified;
    }

    public int getVerified() {
        return this.verified;
    }
}
