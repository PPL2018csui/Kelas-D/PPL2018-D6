package com.exteam.e_xhibisi.data;

public class Product {

    private String exhibit_id;
    private String exhibit_name;
    private String icon;

    public void setId(String id) {
        this.exhibit_id = id;
    }

    public String getId() {
        return this.exhibit_id;
    }

    public void setName(String name) {
        this.exhibit_name = name;
    }

    public String getName() {
        return this.exhibit_name;
    }

    public void setIcon(String url) {
        this.icon = url;
    }

    public String getIcon() {
        return this.icon;
    }
}
