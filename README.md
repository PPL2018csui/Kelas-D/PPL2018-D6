# E-XHIBISI

> Project by : Annisa Fitri S  
> Menyediakan jasa vote secara online untuk mata kuliah PPL Fasilkom UI 2018

## Getting Started

### Webservice (API) 

Silahkan download Ruby sesuai urutan di sini (Jangan lupa menginstall curl dengan perintah "sudo apt-get install curl").
```
curl -L get.rvm.io | bash -s stable
source ~/.bash_profile
rvm requirements
rvm install 2.3.0
```

Apabila pada ./bash profile tidak dapat dilakukan silahkan mencoba:
```
source ~/.profile
```

###### Langkah-Langkah Install Repository 

Download & Setting GIT

[Git]
```
sudo apt-get install git
git config --global user.name "John Appleseed"
git config --global user.email "email@example.com"
```
Clone repository PPLD6 di gitlab
```
git clone https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6.git E-XHIBISI
cd E-XHIBISI
```

###### Install Database
The following command can be used to install MongoDB on a Debian-based Linux.
```
$ sudo apt-get install mongodb
```
The command installs the necessary packages that come with MongoDB.
```
$ sudo service mongodb status
mongodb start/running, process 975
```
With the sudo service mongodb status command we check the status of the mongodb server.
```
$ sudo service mongodb start
mongodb start/running, process 6448
```
The mongodb server is started with the sudo service mongodb start command.
```
$ sudo gem install mongo
```
The MongoDB Ruby driver is installed with sudo gem install mongo command. 

###### Set-up Local Environment
Setting Environment Ruby (Jangan lupa menginstall bundler dengan perintah "sudo gem install bundler").
```
rvm gemset create E-XHIBISI
rvm 2.3.0@E-XHIBISI
cd API
bundle install
```

Run Application with :
```
shotgun config.ru
```
For Debug Console use :
```
racksh
```

## Running the tests

For Webservice you can run your test using :
```
cd API
rspec spec
```

## Deployment

In E-XHIBISI we use automatic deployment on gitlab using gitlab.ci, all you have to do is push your current work branch itu your branch. 
If you prefer to test your code locally you can do the following :

##### Using Gitlab Multi Runner

Install the following dependency, keep in mind the platform you are using (Mac, Ubuntu, Windows) :

```
$ docker --version
$ docker-compose --version
$ docker-machine --version
```

And lastly install this using your current platform 

[Gitlab Runner](https://docs.gitlab.com/runner/install/linux-manually.html)

After then you can run your test at gilab locally using the following command

```
gitlab-ci-multi-runner exec docker unitTest:API
```

## Built With

[Sinatra](http://sinatrarb.com/)

[Mongoid](https://docs.mongodb.com/mongoid/master/#ruby-mongoid-tutorial)

[MVC Sinatra & Mongoid](https://blog.openshift.com/spatial-apps-on-openshift-paas-using-ruby-sinatra-and-mongodb/)

## Author

###### The E-XTeam
- Aji Imawan Omi
- Bagas Prasetya Adi 
- Wilda Septiani 
- Neneng Vela 
- Troy Amadeus 
- Emil Farisan

## Pipeline

Status Master :

Pipeline : [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/badges/master/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/commits/master) 
Coverage : [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/badges/master/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/commits/master)

Status SIT_UAT :

Pipeline : [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/badges/master/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/commits/sit_uat) 
Coverage : [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/badges/master/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D6/commits/sit_uat)

## Version

Currently in version 1.0

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
